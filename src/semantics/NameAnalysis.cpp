#include <algorithm>
#include <iostream>

#include "NameAnalysis.h"
#include "Environment.h"

#include "../ast/Ast.h"

using namespace std;


NameAnalysis::NameAnalysis ():
m_isValid (true),
m_mangleID (0), // gets incremented before first use
m_pEnvironment (new Environment ()),
m_errorCount (0),
m_pCurrentClass (nullptr)
{}


bool NameAnalysis::analyze (Tree *pAst, const string& filename) {
  m_errorCount = 0;
  m_filename = filename;
  m_basename = filename.substr (filename.find_last_of ('/')+1);
  m_input = fstream (m_filename);
  
  pAst->accept (this);
  
  Environment *pGlobalEnvironment = m_pEnvironment;
  
  for (auto p : m_pEnvironment->classes) {
    Class *pClass = p.second;
    
    if (pClass->pMainClass) {
      
    } else {
      m_pEnvironment = pClass->pEnvironment;
      m_pCurrentClass = pClass;
      m_isClassField = true;
      for (auto v : pClass->pAst->varDecls) {
        v->accept (this);
      }
      m_isClassField = false;
      for (auto m : pClass->pAst->methodDecls) {
        m->accept (this);
      }
      m_pEnvironment = m_pEnvironment->leaveScope ();
      
      for (auto m : pClass->pEnvironment->methods) {
        Method *pMethod = m.second;
        
        Environment *oldEnv = m_pEnvironment;
        m_pEnvironment = pMethod->pEnvironment;
        for (auto p : pMethod->pAst->params) {
          p->type->accept (this);
          Variable *pVar = m_pEnvironment->tryDeclareVariable (p->name->name, nullptr, m_mangleID);
          if (! pVar) {
            // This should never happen, but just in case...
            printError ("Invalid redeclaration of variable '" + p->name->name + "'", pMethod->pAst->token.line, pMethod->pAst->token.column);
            p->name->mangledName += "_#error#_";
          } else {
            ++m_mangleID;
            p->name->mangledName += "_" + to_string (pVar->mangleID) + "_";
            pVar->pAst = p;
            pMethod->params [p->name->name] = pVar;
            pMethod->paramOrder.push_back (p->name->name);
          }
        }
        
        if (!pClass->pMainClass) {
          pMethod->pAst->returnType->accept (this);
        }
        
        m_pEnvironment = oldEnv;
      }
    }
  }
  
  // Check base classes
  for (auto p : m_pEnvironment->classes) {
    Class *pClass = p.second;
    if (pClass->pAst) {
      const string& baseClassName = pClass->pAst->baseClassName->name;
      if (baseClassName != "") {
        auto it = m_pEnvironment->classes.find (baseClassName);
        if (it != m_pEnvironment->classes.end ()) {
          if (it->second->pMainClass) {
            printError ("Class '" + p.first + "' can't inherit from the main class", pClass->pAst->token.line, pClass->pAst->token.column);
            continue;
          }
          if (pClass == it->second) {
            printError ("Class '" + p.first + "' can't inherit from itself", pClass->pAst->token.line, pClass->pAst->token.column);
            continue;
          }
          pClass->pBaseClass = it->second;
          pClass->pAst->baseClassName->mangledName += "_" + to_string (pClass->pBaseClass->mangleID) + "_";
        } else {
          printError ("Use of unresolved base class name '" + baseClassName + "'", pClass->pAst->token.line, pClass->pAst->token.column);
          continue;
        }
        
        // TODO: Recursively check base class hierarchy
        // Check overriding methods from base class
        for (auto m : pClass->pEnvironment->methods) {
          Method *pMethod = m.second;
          auto it = pClass->pBaseClass->pEnvironment->methods.find (m.first);
          if (it != pClass->pBaseClass->pEnvironment->methods.end ()) {
            Method *pBaseMethod = it->second;
            if (pMethod->params.size () != pBaseMethod->params.size ()) {
              printError ("Overriden method '" + m.first + "' of class '" + p.first + "' doesn't match signature in base class '" + baseClassName + "'", pMethod->pAst->token.line, pMethod->pAst->token.column);
              continue;
            }
          }
        }
        
        // Check overriding data members (not allowed)
        for (auto v : pClass->pEnvironment->vars) {
          auto it = pClass->pBaseClass->pEnvironment->vars.find (v.first);
          if (it != pClass->pBaseClass->pEnvironment->vars.end ()) {
            printError ("Member variable '" + v.first + "' overrides member of base class '" + baseClassName + "'", v.second->pAst->token.line, v.second->pAst->token.column);
            continue;
          }
        }
      }
    }
  }
  
  // Check for inheritance cycle
  set<string> detectedCycles;
  for (auto p : m_pEnvironment->classes) {
    Class *pClass = p.second;
    
    Class *pC = pClass->pBaseClass;
    vector<string> hierarchy = {p.first};
    while (pC) {
      if (detectedCycles.find (pC->name) != detectedCycles.end ()) {
        break;
      }
      if (pC == pClass) {
        detectedCycles.insert (pClass->name);
        string cycle;
        for (const string& s : hierarchy) {
          cycle += s + " -> ";
        }
        cycle += p.first;
        printError ("Inheritance cycle discovered: " + cycle, pClass->pAst->token.line, pClass->pAst->token.column);
        break;
      }
      if (find (hierarchy.begin (), hierarchy.end (), pC->name) != hierarchy.end ()) {
        break;
      }
      hierarchy.push_back (pC->name);
      pC = pC->pBaseClass;
    }
  }
  
  // Evaluate method bodies
  for (auto p : m_pEnvironment->classes) {
    for (auto m : p.second->pEnvironment->methods) {
      if (m.second->pAst) {
        m_pEnvironment = m.second->pEnvironment;
        
        for (auto v : m.second->pAst->varDecls) {
          v->accept (this);
        }
        for (auto s : m.second->pAst->body) {
          s->accept (this);
        }
        // Main method doesn't have return
        if (m.second->pAst->returnExpr) {
          m.second->pAst->returnExpr->accept (this);
        }
        
        m_pEnvironment = m_pEnvironment->leaveScope ();
      } else {
        cerr << "Method '" + m.first + "' not correctly processed" << endl;
      }
    }
  }
  
  m_pEnvironment = pGlobalEnvironment;
  
  return m_isValid;
}


void NameAnalysis::printError (const string& message, int line, int column) {
  m_isValid = false;
  cout << "\033[0;33m" << line << ":" << column << ":" << m_basename << "\033[1;31m error: \033[0m" << message << endl;
  
  ++m_errorCount;
  
  readLine (line-1, [column](const string& line) {
    string noTabs = line;
    string::size_type n = 0;
    string s = "\t";
    string t = "    ";
    while ((n = noTabs.find (s, n)) != string::npos) {
      noTabs.replace (n, s.size (), t);
      n += t.size();
    }
    
    cout << "    " << noTabs << endl;
    cout << "    ";
    cout << string(column-1, ' ');
    cout << '^';
    //cout << string(sourceLoc.col.end - sourceLoc.col.start, '~');
    cout << endl;
  });
}

void NameAnalysis::readLine(int targetLine, std::function<void(const string&)> handler) {
  // We are going to be reading from the input file, so seek back to the
  // beginning of the file.
  m_input.clear();
  m_input.seekg(0, ios::beg);
  
  int lineNum = 0;
  string line;
  
  while (getline(m_input, line)) {
    if (lineNum == targetLine) {
      handler(line);
      break;
    }
    ++lineNum;
  }
}


// Expressions
void NameAnalysis::visit (Ident* e) {
  Variable *pVar = m_pEnvironment->findVar (e->name);
  if (pVar) {
    e->mangledName += "_" + to_string (pVar->mangleID) + "_";
    e->pClass = pVar->pClass;
  } else {
    printError ("Use of unresolved identifier '" + e->name + "'", e->token.line, e->token.column);
    e->mangledName += "_#error#_";
  }
}

void NameAnalysis::visit (IntLiteral* e) {}
void NameAnalysis::visit (StringLiteral* e) {}
void NameAnalysis::visit (BoolLiteral *e) {}

void NameAnalysis::visit (Length *e) {
  e->expr->accept (this);
}
void NameAnalysis::visit (MethodCall *e) {
  e->expr->accept (this);
  e->methodName->mangledName += "_#error#_";
  for (auto p : e->params) {
    p->accept (this);
  }
}

void NameAnalysis::visit (New *e) {
  Class *pClass = m_pEnvironment->findClass (e->ID->name);
  if (pClass) {
    e->ID->mangledName += "_" + to_string (pClass->mangleID) + "_";
  } else {
    printError ("Use of unresolved type name '" + e->ID->name + "'", e->token.line, e->token.column);
    e->ID->mangledName += "_#error#_";
  }
}
void NameAnalysis::visit (NewIntArray *e) {
  e->expr->accept (this);
}

void NameAnalysis::visit (Subscript *e) {
  e->expr->accept (this);
  e->subscript->accept (this);
}
void NameAnalysis::visit (This *e) {
  if (m_pEnvironment->pCurrentClass->pMainClass) {
    printError ("'this' cannot be referenced from the main class", e->token.line, e->token.column);
  }
}

// Binary Operators
void NameAnalysis::visitBinaryOperator (std::string op, BinaryOp *expr) {
  expr->lhs->accept (this);
  expr->rhs->accept (this);
}
void NameAnalysis::visit(Plus* e) {
  visitBinaryOperator ("+", e);
}
void NameAnalysis::visit(Minus* e) {
  visitBinaryOperator ("-", e);
}
void NameAnalysis::visit(Times* e) {
  visitBinaryOperator ("*", e);
}
void NameAnalysis::visit(Division* e) {
  visitBinaryOperator ("/", e);
}
void NameAnalysis::visit(Equals* e) {
  visitBinaryOperator ("==", e);
}
void NameAnalysis::visit(LessThan* e) {
  visitBinaryOperator ("<", e);
}
void NameAnalysis::visit(And* e) {
  visitBinaryOperator ("&&", e);
}
void NameAnalysis::visit(Or* e) {
  visitBinaryOperator ("||", e);
}

// Unary Operators
void NameAnalysis::visitUnaryOperator (std::string op, UnaryOp *expr) {
  expr->expr->accept (this);
}
void NameAnalysis::visit(Not* e) {
  visitUnaryOperator ("!", e);
}


// Statements
void NameAnalysis::visit(IfThenElse* s) {
  s->expr->accept (this);
  s->then->accept (this);
  if (s->elze) {
    s->elze->accept (this);
  }
}

void NameAnalysis::visit(Print* s) {
  s->expr->accept (this);
}

void NameAnalysis::visit(Assign* s) {
  Variable *pVar = m_pEnvironment->findVar (s->varID->name);
  if (pVar) {
    s->varID->mangledName += "_" + to_string (pVar->mangleID) + "_";
    s->varID->pClass = pVar->pClass;
  } else {
    printError ("Use of unresolved identifier '" + s->varID->name + "'", s->token.line, s->token.column);
    s->varID->mangledName += "_#error#_";
  }
  if (s->subscript) {
    s->subscript->accept (this);
  }
  s->expr->accept (this);
}

void NameAnalysis::visit(While* s) {
  s->expr->accept (this);
  s->body->accept (this);
}

void NameAnalysis::visit(For* s) {
  s->init->accept (this);
  s->expr->accept (this);
  s->step->accept (this);
  s->body->accept (this);
}

void NameAnalysis::visit(Block* block) {
  for (auto s : block->body) {
    s->accept (this);
  }
}

void NameAnalysis::visit (Sidef *s) {
  s->expr->accept (this);
}


// Types
void NameAnalysis::visit (BoolType *t) {
}
void NameAnalysis::visit (IDType *t) {
  Class *pClass = m_pEnvironment->findClass (t->ID->name);
  if (pClass) {
    t->ID->mangledName += "_" + to_string (pClass->mangleID) + "_";
  } else {
    printError ("Use of unresolved type name '" + t->ID->name + "'", t->token.line, t->token.column);
    t->ID->mangledName += "_#error#_";
  }
}
void NameAnalysis::visit (IntArrayType *t) {
}
void NameAnalysis::visit (IntType *t) {
}
void NameAnalysis::visit (StringType *t) {
}


// Other

void NameAnalysis::visit (Program *n) {
  n->mainClass->accept (this);
  for (auto c : n->classDecls) {
    c->accept (this);
  }
}

void NameAnalysis::visit (MainClass *n) {
  Class *pClass = m_pEnvironment->tryDeclareClass (n->className->name, m_pEnvironment, m_mangleID);
  if (! pClass) {
    // Main class will always be declared first, so we don't really need to worry
    // about other class with same name at this point.
    printError ("Invalid redeclaration of class '" + n->className->name + "'", n->token.line, n->token.column);
    n->className->mangledName += "_#error#_";
  }
  
  ++m_mangleID;
  n->className->mangledName += "_" + to_string (pClass->mangleID) + "_";
  
  pClass->pMainClass = n;
  
  m_pEnvironment = pClass->pEnvironment;
  n->mainMethod->accept (this);
  m_pEnvironment = m_pEnvironment->leaveScope ();
}

void NameAnalysis::visit (ClassDecl *n) {
  Class *pClass = m_pEnvironment->tryDeclareClass (n->className->name, m_pEnvironment, m_mangleID);
  if (! pClass) {
    printError ("Invalid redeclaration of class '" + n->className->name + "'", n->token.line, n->token.column);
    n->className->mangledName += "_#error#_";
    return;
  }
  
  ++m_mangleID;
  n->className->mangledName += "_" + to_string (pClass->mangleID) + "_";
  
  pClass->pAst = n;
}

void NameAnalysis::visit (MethodDecl *n) {
  Method *pMethod = m_pEnvironment->tryDeclareMethod (n->methodName->name, m_pEnvironment, m_mangleID);
  if (! pMethod) {
    printError ("Invalid redeclaration of method '" + n->methodName->name + "'", n->token.line, n->token.column);
    if (! m_pEnvironment->pCurrentClass->pMainClass) {
      n->methodName->mangledName += "_#error#_";
    }
    return;
  }
  
  // Don't mangle the main method name
  if (! m_pEnvironment->pCurrentClass->pMainClass) {
    ++m_mangleID;
    n->methodName->mangledName += "_" + to_string (pMethod->mangleID) + "_";
  }
  
  pMethod->pAst = n;
}

void NameAnalysis::visit (VarDecl *n) {
  n->type->accept (this);
  Variable *pVar = m_pEnvironment->tryDeclareVariable (n->name->name, m_isClassField ? m_pCurrentClass : nullptr, m_mangleID);
  if (! pVar) {
    printError ("Invalid redeclaration of variable '" + n->name->name + "'", n->token.line, n->token.column);
    n->name->mangledName += "_#error#_";
    return;
  }
  
  ++m_mangleID;
  n->name->mangledName += "_" + to_string (pVar->mangleID) + "_";
  
  pVar->pAst = n;
}
