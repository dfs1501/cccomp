#ifndef LENGTH_H
#define LENGTH_H

#include "Expression.h"

using namespace std;

class Length : public Expression {
public:
  Expression *expr;
  
  Length (Expression *expr):
  expr (expr)
  {}
  
  ACCEPT_VISITOR
};

#endif
