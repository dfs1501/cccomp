#ifndef WHILE_H
#define WHILE_H

#include "../expr/Expression.h"
#include "Statement.h"

class While : public Statement {
public:
  Expression*  expr;
  Statement*   body;

  While(Expression* expr, Statement* body) {
    this->expr = expr;
    this->body = body;
  }
  
  ACCEPT_VISITOR
};

#endif