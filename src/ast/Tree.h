#ifndef TREE_H
#define TREE_H

#define ACCEPT_VISITOR void accept(Visitor *v) override { v->visit(this); }

#include "Visitor.h"
#include "../lexer/tokens.h"

class TType;

class Tree {
public:
  Token token;
  TType *pType;

public:
	virtual void accept(Visitor *v) = 0;
};

#endif
