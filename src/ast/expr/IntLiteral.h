#ifndef INTLITERAL_H
#define INTLITERAL_H

#include "Expression.h"

class IntLiteral : public Expression {
public:
  int value;
  
  IntLiteral(int value) {
    this->value = value;
  }

  ACCEPT_VISITOR
};

#endif