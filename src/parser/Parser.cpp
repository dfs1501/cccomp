#include <vector>
#include <string>
#include <iostream>

#include "Parser.h"

#include "../ast/Ast.h"

using namespace std;

// A define for debugging purposes. Prints the file and line number that called
// the expect when in debug mode.
#define __expect(A) expect(A, "", std::string(__FILE__), __LINE__)


Program* Parser::parse (FILE *in) {
  lexBegin (in);
  return parseProgram ();
}

Token Parser::expect (TokenType expected, string errorMessage /*= ""*/, string file/*= ""*/, int line /*= 0*/) {
  Token tk = lexGetNext ();
  if (tk.type != expected) {
    if (errorMessage == "") {
      errorMessage = "unexpected '" + TokenTypeToString [tk.type] + "', expecting '" + TokenTypeToString [expected] + "'";
      #ifdef __DEBUG
      errorMessage += " <" + file + ":" + to_string (line) + ">";
      #endif
    }
    throw ParseError (tk, errorMessage);
  }
  return tk;
}

/**
 * Program ::= MainClass (ClassDeclaration)* <EOF>
 */
Program* Parser::parseProgram () {
  Program* parsedProgram = new Program (
    // MainClass
    parseMainClass (),
    
    // (ClassDeclaration)* <EOF>
    zeroOrMore<ClassDecl*> (
      [this](){ return parseClassDeclaration (); }, 
      {TokenType::TK_EOF}
      )
    );
  parsedProgram->token = parsedProgram->mainClass->token;
  return parsedProgram;
}

/**
 * MainClass ::= class Identifier { public static void main ( String [ ] Identifier ) { Statement } }
 */
MainClass* Parser::parseMainClass () {
  MainClass *mainClass = new MainClass ();
  
  // class
  Token tkClass = __expect (TokenType::TK_CLASS);
  mainClass->token = tkClass;
  
  // Identifier
  Token tkClassName = __expect (TokenType::TK_ID);
  mainClass->className = new Ident (tkClassName.data);
  mainClass->className->token = tkClassName;
  
  MethodDecl *mainMethod = new MethodDecl ();
  mainMethod->returnType = new IDType ("static void");
  mainMethod->methodName = new Ident ("main");
  mainMethod->returnExpr = nullptr;
  
  // { public static void main ( String [ ]
  __expect (TokenType::TK_LBRACE);
  mainMethod->token = __expect (TokenType::TK_PUBLIC);
  __expect (TokenType::TK_STATIC);
  __expect (TokenType::TK_VOID);
  __expect (TokenType::TK_MAIN);
  __expect (TokenType::TK_LPAREN);
  __expect (TokenType::TK_STRING);
  __expect (TokenType::TK_LBRACK);
  __expect (TokenType::TK_RBRACK);
  
  // Identifier
  Token tkArgsName = __expect (TokenType::TK_ID);
  // TODO: This string type should be string[] when we have type checking
  mainMethod->params.push_back (new VarDecl (new StringType (), tkArgsName.data));
  mainMethod->params[0]->token = tkArgsName;
  
  // ) {
  __expect (TokenType::TK_RPAREN);
  __expect (TokenType::TK_LBRACE);
  
  // Statement
  mainMethod->body = { parseStatement () };
  
  // } }
  __expect (TK_RBRACE);
  __expect (TK_RBRACE);
  
  mainClass->mainMethod = mainMethod;
  
  return mainClass;
}

/**
 * ClassDeclaration ::= class Identifier (extends Identifier)? { (VarDeclaration)* (MethodDeclaration)* }
 */
ClassDecl* Parser::parseClassDeclaration () {
  ClassDecl *classDecl = new ClassDecl ();
  
  // class
  Token classToken = __expect (TokenType::TK_CLASS);
  classDecl->token = classToken;

  // Identifier
  Token className = __expect (TokenType::TK_ID);
  classDecl->className = new Ident (className.data);
  classDecl->className->token = className;
  
  // ?
  if (lexPeekNext ().type == TokenType::TK_EXTENDS) {
    // extends
    __expect (TokenType::TK_EXTENDS);
    
    // Identifier
    Token baseClassName = __expect (TokenType::TK_ID);
    classDecl->baseClassName = new Ident (baseClassName.data);
    classDecl->baseClassName->token = baseClassName;
  }
  else {
    classDecl->baseClassName = new Ident ("");
  }
  
  // {
  __expect (TokenType::TK_LBRACE);
  
  // (VarDeclaration)*
  // FIRST(MethodDeclaration) = { TK_PUBLIC }
  classDecl->varDecls = zeroOrMore<VarDecl*> (
      [this](){ return parseVarDeclaration (); }, 
      {TokenType::TK_PUBLIC, TokenType::TK_RBRACE},
      false // Don't consume the public token
      );
  
  // (MethodDeclaration)* }
  classDecl->methodDecls = zeroOrMore<MethodDecl*> (
      [this](){ return parseMethodDeclaration (); }, 
      {TokenType::TK_RBRACE}
      );
  
  return classDecl;
}

/**
 * VarDeclaration ::= Type Identifier ;
 */
VarDecl* Parser::parseVarDeclaration () {
  // Type
  Type *type = parseType ();
  // Identifier
  Token ID = __expect (TokenType::TK_ID);
  // ;
  __expect (TokenType::TK_SEMICOLON);
  VarDecl* var = new VarDecl (type, ID.data);
  var->token = type->token;
  return var;
}

/**
 * MethodDeclaration ::= public Type Identifier ( (Type Identifier (, Type Identifier)*)? ) { (VarDeclaration)* (Statement)* return Expression ; }
 */
MethodDecl* Parser::parseMethodDeclaration () {
  MethodDecl *methodDecl = new MethodDecl ();
  
  // public
  Token publicToken = __expect (TokenType::TK_PUBLIC);
  methodDecl->token = publicToken;

  // Type
  methodDecl->returnType = parseType ();
  
  // Identifier
  Token name = __expect (TokenType::TK_ID);
  methodDecl->methodName = new Ident (name.data);
  methodDecl->methodName->token = name;
  
  // (
  __expect (TokenType::TK_LPAREN);
  
  // ?
  if (lexPeekNext ().type != TokenType::TK_RPAREN) {
    // Type Identifier
    Type *type = parseType ();
    Token name = __expect (TokenType::TK_ID);
    methodDecl->params.push_back (new VarDecl (type, name.data));
    methodDecl->params[0]->token = name;
    
    // (, Type Identifier)* )
    zeroOrMore<VarDecl*> ([this,methodDecl]() {
      __expect (TokenType::TK_COMMA);
      Type *type = parseType ();
      Token name = __expect (TokenType::TK_ID);
      VarDecl *param = new VarDecl (type, name.data);
      methodDecl->params.push_back (param);
      return param;
    }, {TokenType::TK_RPAREN});
  } else {
    // )
    __expect (TokenType::TK_RPAREN);
  }
  
  // {
  __expect (TokenType::TK_LBRACE);
  
  // (VarDeclaration)*
  // FIRST(Statement) = { int, bool, String, Identifier }
  VarDecl *varDecl = nullptr;
  do {
    if (!lexHasNext ()) {
        // Throw error
        std::string errorMessage = "unexpected end of file";
#ifdef __DEBUG
        errorMessage += "<" + std::string(__FILE__) + ":" + to_string (__LINE__) + ">";
#endif
        throw ParseError (lexPeekNext (), errorMessage);
        break;
    }
    Token tok = lexPeekNext ();
    if (tok.type != TokenType::TK_INT &&
        tok.type != TokenType::TK_BOOLEAN &&
        tok.type != TokenType::TK_STRING &&
        // Use lookahead to check if we have a statement or another variable declaration
        (tok.type != TokenType::TK_ID || lexLookahead ().type != TokenType::TK_ID))
    {
      break;
    }
    varDecl = parseVarDeclaration ();
    methodDecl->varDecls.push_back (varDecl);
  } while (varDecl);
  
  // (Statement)* return
  methodDecl->body = zeroOrMore<Statement*> (
    [this]() { return parseStatement (); },
    {TokenType::TK_RETURN}
    );
  
  // Expression
  methodDecl->returnExpr = parseExpression ();
  
  // ; }
  __expect (TokenType::TK_SEMICOLON);
  __expect (TokenType::TK_RBRACE);
  
  return methodDecl;
}

/**
 * Type ::= int
 *        | boolean
 *        | String
 *        | int [ ]
 *        | Identifier
 */
Type* Parser::parseType () {
  Type* parsedType;
  Token tok = lexGetNext ();
  if (tok.type == TokenType::TK_INT) {
    if (lexPeekNext ().type == TokenType::TK_LBRACK) {
      __expect (TokenType::TK_LBRACK);
      __expect (TokenType::TK_RBRACK);
      parsedType =  new IntArrayType ();
      parsedType->token = tok;
      return parsedType;
    } else {
      parsedType =  new IntType ();
      parsedType->token = tok;
      return parsedType;
    }
  }
  else if (tok.type == TokenType::TK_BOOLEAN) {
    parsedType =  new BoolType ();
    parsedType->token = tok;
    return parsedType;
  }
  else if (tok.type == TokenType::TK_STRING) {
    parsedType =  new StringType ();
    parsedType->token = tok;
    return parsedType;
  }
  else if (tok.type == TokenType::TK_ID) {
    parsedType =  new IDType (tok.data);
    parsedType->token = tok;
    return parsedType;
  }
  else {
    std::string errorMessage = "unexpected " + TokenTypeToString [tok.type] +
      ", expected token of type int, int[], boolean, String, or an identifier";
#ifdef __DEBUG
    errorMessage += " <" + std::string(__FILE__) + ":" + to_string(__LINE__) + ">";
#endif
    throw ParseError(tok, errorMessage);
  }
}

/**
 * Statement ::= { (Statement)* }
 *             | if ( Expression ) Statement (else Statement)?
 *             | while ( Expression ) Statement
 *             | System.out.println ( Expression ) ;
 *             | Identifier = Expression ;
 *             | Identifier [ Expression ] = Expression ;
 *             | sidef ( Expression ) ;
 */
Statement* Parser::parseStatement () {
  Statement* parsedStatement;
  Token tok = lexGetNext ();
  // { (Statement)* }
  if (tok.type == TokenType::TK_LBRACE) {
    parsedStatement = new Block (zeroOrMore<Statement*> (
      [this](){ return parseStatement (); }, 
      {TokenType::TK_RBRACE}
      ));
    parsedStatement->token = tok;
    return parsedStatement;
  }
  // if ( Expression ) Statement (else Statement)?
  else if (tok.type == TokenType::TK_IF) {
    __expect (TokenType::TK_LPAREN);
    Expression *cond = parseExpression ();
    __expect (TokenType::TK_RPAREN);
    Statement *then = parseStatement ();
    Statement *elze = nullptr;
    if (lexPeekNext ().type == TokenType::TK_ELSE) {
      __expect (TokenType::TK_ELSE);
      elze = parseStatement ();
    }
    parsedStatement = new IfThenElse (cond, then, elze);
    parsedStatement->token = tok;
    return parsedStatement;
  }
  // while ( Expression ) Statement
  else if (tok.type == TokenType::TK_WHILE) {
    __expect (TokenType::TK_LPAREN);
    Expression *cond = parseExpression ();
    __expect (TokenType::TK_RPAREN);
    Statement *body = parseStatement ();
    parsedStatement = new While (cond, body);
    parsedStatement->token = tok;
    return parsedStatement;
  }
  // System.out.println ( Expression ) ;
  else if (tok.type == TokenType::TK_PRINTLN) {
    __expect (TokenType::TK_LPAREN);
    Expression *expr = parseExpression ();
    __expect (TokenType::TK_RPAREN);
    __expect (TokenType::TK_SEMICOLON);
    parsedStatement = new Print (expr);
    parsedStatement->token = tok;
    return parsedStatement;
  }
  else if (tok.type == TokenType::TK_ID) {
    string name = tok.data;
    Token next = lexGetNext ();
    // Identifier = Expression ;
    if (next.type == TokenType::TK_EQ) {
      Expression *expr = parseExpression ();
      __expect (TokenType::TK_SEMICOLON);
      parsedStatement = new Assign (name, expr);
      parsedStatement->token = tok;
      return parsedStatement;
    }
    // Identifier [ Expression ] = Expression ;
    else if (next.type == TokenType::TK_LBRACK) {
      Expression *subscript = parseExpression ();
      __expect (TokenType::TK_RBRACK);
      __expect (TokenType::TK_EQ);
      Expression *expr = parseExpression ();
      __expect (TokenType::TK_SEMICOLON);
      parsedStatement = new Assign (name, expr, subscript);
      parsedStatement->token = tok;
      return parsedStatement;
    }
    else {
      std::string errorMessage = "unexpected "+  TokenTypeToString[tok.type] +
        " in statement";
#ifdef __DEBUG
      errorMessage += " <" +  std::string(__FILE__) + ":" + to_string(__LINE__) + ">";
#endif
      throw ParseError(tok, errorMessage);
    }
  }
  // sidef ( Expression ) ;
  else if (tok.type == TokenType::TK_SIDEF) {
    __expect (TokenType::TK_LPAREN);
    Expression *expr = parseExpression ();
    __expect (TokenType::TK_RPAREN);
    __expect (TokenType::TK_SEMICOLON);
    parsedStatement = new Sidef (expr);
    parsedStatement->token = tok;
    return parsedStatement;
  }
  
  string errorMessage = "unexpected " + TokenTypeToString [tok.type] + " in statement";
#ifdef __DEBUG
  errorMessage += " <" + std::string(__FILE__) + ":" + to_string(__LINE__) + ">";
#endif
  throw ParseError (tok, errorMessage);
}

/**
 * or -> and '||' or | and
 * and -> comp '&&' and | comp
 * comp -> plus ('<' || '==') comp | plus
 * plus -> times ('+' || '-') plus | times
 * times -> not ('*' || '/') times | not
 * not -> '!' atom | atom
 * atom -> intlit
 */

/**
 * Updated Expression grammar with left recursion removed.
 *
 * Expression  ::= OrExp
 * OrExp       ::= AndExp (|| OrExp)?
 * AndExp      ::= CompExp (&& AndExp)?
 * CompExp     ::= PlusExp ((< | ==) CompExp)?
 * PlusExp     ::= TimesExp ((+ | -) PlusExp)?
 * TimesExp    ::= NotExp ((* | /) TimesExp)?
 * NotExp      ::= (!)? NotExp | PostfixExp
 * PostfixExp  ::= Atom PostfixTail*
 * PostfixTail ::= . length
 *               | . Identifier ( (Expression (, Expression)*)? )
 *               | [ Expression ]
 * Atom        ::= <INTEGER_LITERAL>
 *               | "<STRING_LITERAL>"
 *               | true
 *               | false
 *               | Identifier
 *               | this
 *               | new int [ Expression ]
 *               | new Identifier ( )
 *               | ( Expression )
 */
Expression* Parser::parseExpression () {
  return parseOrExp ();
}
Expression* Parser::parseOrExp () {
  Expression *lhs = parseAndExp ();
  if (lexPeekNext ().type == TokenType::TK_OR) {
    Token tok = __expect (TokenType::TK_OR);
    Expression *rhs = parseOrExp ();
    Expression *parsedOr = new Or (lhs, rhs);
    parsedOr->token = tok;
    return parsedOr;
  } else {
    return lhs;
  }
}
Expression* Parser::parseAndExp () {
  Expression *lhs = parseCompExp ();
  if (lexPeekNext ().type == TokenType::TK_AND) {
    Token tok = __expect (TokenType::TK_AND);
    Expression *rhs = parseAndExp ();
    Expression *parsedAnd = new And (lhs, rhs);
    parsedAnd->token = tok;
    return parsedAnd;
  } else {
    return lhs;
  }
}
Expression* Parser::parseCompExp () {
  Expression *lhs = parsePlusExp ();
  if (lexPeekNext ().type == TokenType::TK_LESSTHAN) {
    Token tok = __expect (TokenType::TK_LESSTHAN);
    Expression *rhs = parseCompExp ();
    Expression *parsedLessThan = new LessThan (lhs, rhs);
    parsedLessThan->token = tok;
    return parsedLessThan;
  } else if (lexPeekNext ().type == TokenType::TK_EQEQ) {
    Token tok = __expect (TokenType::TK_EQEQ);
    Expression *rhs = parseCompExp ();
    Expression *parsedEquals = new Equals (lhs, rhs);
    parsedEquals->token = tok;
    return parsedEquals;
  } else {
    return lhs;
  }
}
Expression* Parser::parsePlusExp () {
  Expression *lhs = parseTimesExp ();
  if (lexPeekNext ().type == TokenType::TK_PLUS) {
    Token tok = __expect (TokenType::TK_PLUS);
    Expression *rhs = parsePlusExp ();
    Expression *parsedPlus = new Plus (lhs, rhs);
    parsedPlus->token = tok;
    return parsedPlus;
  } else if (lexPeekNext ().type == TokenType::TK_MINUS) {
    Token tok = __expect (TokenType::TK_MINUS);
    Expression *rhs = parsePlusExp ();
    Expression *parsedMinus = new Minus (lhs, rhs);
    parsedMinus->token = tok;
    return parsedMinus;
  } else {
    return lhs;
  }
}
Expression* Parser::parseTimesExp () {
  Expression *lhs = parseNotExp ();
  if (lexPeekNext ().type == TokenType::TK_TIMES) {
    Token tok = __expect (TokenType::TK_TIMES);
    Expression *rhs = parseTimesExp ();
    Expression *parsedTimes = new Times (lhs, rhs);
    parsedTimes->token = tok;
    return parsedTimes;
  } else if (lexPeekNext ().type == TokenType::TK_DIV) {
    Token tok = __expect (TokenType::TK_DIV);
    Expression *rhs = parseTimesExp ();
    Expression *parsedDivision = new Division (lhs, rhs);
    parsedDivision->token = tok;
    return parsedDivision;
  } else {
    return lhs;
  }
}
Expression* Parser::parseNotExp () {
  if (lexPeekNext ().type == TokenType::TK_BANG) {
    Token tok = __expect (TokenType::TK_BANG);
    Expression *parsedNot = new Not (parseNotExp ());
    parsedNot->token = tok;
    return parsedNot;
  } else {
    return parsePostfixExp ();
  }
}
Expression* Parser::parsePostfixExp () {
  Expression *atom = parseAtom ();
  while (lexPeekNext ().type == TokenType::TK_DOT ||
         lexPeekNext ().type == TokenType::TK_LBRACK)
  {
    if (lexPeekNext ().type == TokenType::TK_DOT) {
      __expect (TokenType::TK_DOT);
      Token tok = lexGetNext ();
      if (tok.type == TokenType::TK_LENGTH) {
        atom = new Length (atom);
        atom->token = tok;
      }
      else if (tok.type == TokenType::TK_ID) {
        MethodCall *methCall = new MethodCall (atom, tok.data, {});
        __expect (TokenType::TK_LPAREN);
        if (lexPeekNext ().type == TokenType::TK_RPAREN) {
          __expect (TokenType::TK_RPAREN);
        } else {
          methCall->params.push_back (parseExpression ());
          zeroOrMore<Expression*> ([this,methCall]() {
            __expect (TokenType::TK_COMMA);
            Expression *expr = parseExpression ();
            methCall->params.push_back (expr);
            return expr;
          }, {TokenType::TK_RPAREN});
        }
        atom = methCall;
        atom->token = tok;
      }
      else {
        std::string errorMessage = "unexpected "+  TokenTypeToString[tok.type] + " after '.'";
#ifdef __DEBUG
        errorMessage += " <" +  std::string(__FILE__) + ":" + to_string(__LINE__) + ">";
#endif
        throw ParseError(tok, errorMessage);
      }
    }
    else if (lexPeekNext ().type == TokenType::TK_LBRACK) {
      Token subToken = __expect (TokenType::TK_LBRACK);
      Expression *subscript = parseExpression ();
      __expect (TokenType::TK_RBRACK);
      atom = new Subscript (atom, subscript);
      atom->token = subToken;
    }
  }
  return atom;
}
Expression* Parser::parseAtom () {
  Expression *parsedAtom;
  Token tok = lexGetNext ();
  if (tok.type == TokenType::TK_INTLIT) {
    parsedAtom = new IntLiteral (stoi (tok.data));
    parsedAtom->token = tok;
    return parsedAtom;
  }
  else if (tok.type == TokenType::TK_STRINGLIT) {
    parsedAtom = new StringLiteral (tok.data);
    parsedAtom->token = tok;
    return parsedAtom;
  }
  else if (tok.type == TokenType::TK_TRUE) {
    parsedAtom = new BoolLiteral (true);
    parsedAtom->token = tok;
    return parsedAtom;
  }
  else if (tok.type == TokenType::TK_FALSE) {
    parsedAtom = new BoolLiteral (false);
    parsedAtom->token = tok;
    return parsedAtom;
  }
  else if (tok.type == TokenType::TK_ID) {
    parsedAtom = new Ident (tok.data);
    parsedAtom->token = tok;
    return parsedAtom;
  }
  else if (tok.type == TokenType::TK_THIS) {
    parsedAtom = new This ();
    parsedAtom->token = tok;
    return parsedAtom;
  }
  else if (tok.type == TokenType::TK_NEW) {
    tok = lexGetNext ();
    if (tok.type == TokenType::TK_INT) {
      // new int [ Expression ]
      __expect (TokenType::TK_LBRACK);
      Expression *expr = parseExpression ();
      __expect (TokenType::TK_RBRACK);
      parsedAtom = new NewIntArray (expr);
      parsedAtom->token = tok;
      return parsedAtom;
    }
    else if (tok.type == TokenType::TK_ID) {
      // new Identifier ( )
      __expect (TokenType::TK_LPAREN);
      __expect (TokenType::TK_RPAREN);
      parsedAtom = new New (tok.data);
      parsedAtom->token = tok;
      return parsedAtom;
    }
    else {
      std::string errorMessage = "unexpected "+  TokenTypeToString[tok.type] +
        " after 'new'";
#ifdef __DEBUG
      errorMessage += " <" +  std::string(__FILE__) + ":" + to_string(__LINE__) + ">";
#endif
      throw ParseError(tok, errorMessage);
    }
  }
  else if (tok.type == TokenType::TK_LPAREN) {
    Expression *expr = parseExpression ();
    __expect (TokenType::TK_RPAREN);
    return expr;
  }
    std::string errorMessage = "unexpected "+  TokenTypeToString[tok.type] +
          " in expression";
  #ifdef __DEBUG
    errorMessage += " <" +  std::string(__FILE__) + ":" + to_string(__LINE__) + ">";
  #endif
    throw ParseError(tok, errorMessage);
}
