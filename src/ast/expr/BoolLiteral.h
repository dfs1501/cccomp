#ifndef BOOL_LITERAL_H
#define BOOL_LITERAL_H

#include "Expression.h"

class BoolLiteral : public Expression {
public:
  bool value;
  
  BoolLiteral (bool value):
  value (value)
  {}
  
  ACCEPT_VISITOR
};

#endif
