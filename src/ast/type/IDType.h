#ifndef ID_TYPE_H
#define ID_TYPE_H

#include <string>

#include "Type.h"
#include "../expr/Ident.h"

using namespace std;

class IDType : public Type {
public:
  Ident *ID;
  
  IDType (const string& ID):
  ID (new Ident (ID))
  {}
  
  ACCEPT_VISITOR
};

#endif
