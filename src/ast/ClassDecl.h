#ifndef CLASS_DECL_H
#define CLASS_DECL_H

#include <string>

#include "Tree.h"
#include "VarDecl.h"
#include "MethodDecl.h"
#include "expr/Ident.h"

using namespace std;

class ClassDecl : public Tree {
public:
  Ident *className;
  Ident *baseClassName;
  vector<VarDecl*> varDecls;
  vector<MethodDecl*> methodDecls;
  
  ClassDecl () {}
  
  ACCEPT_VISITOR
};

#endif
