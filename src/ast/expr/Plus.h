#ifndef PLUS_H
#define PLUS_H

#include "BinaryOp.h"

class Plus : public BinaryOp {
public:
  using BinaryOp::BinaryOp;
  
  ACCEPT_VISITOR
};

#endif