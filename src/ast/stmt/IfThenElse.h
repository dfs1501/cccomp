#ifndef IFTHENELSE_H
#define IFTHENELSE_H

#include "../expr/Expression.h"
#include "Statement.h"

class IfThenElse : public Statement {
public:
  Expression* expr;
  Statement* then;
  Statement* elze;
  
  IfThenElse(Expression* expr, Statement* then, Statement* elze) {
    this->expr = expr;
    this->then = then;
    this->elze = elze;
  }

  ACCEPT_VISITOR
};

#endif