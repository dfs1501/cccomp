#ifndef VISITOR_H
#define VISITOR_H

class Assign;
class Block;
class For;
class IfThenElse;
class Print;
class Sidef;
class While;

class And;
class BoolLiteral;
class Division;
class Equals;
class IntLiteral;
class Length;
class LessThan;
class MethodCall;
class Minus;
class New;
class NewIntArray;
class Not;
class Or;
class Plus;
class StringLiteral;
class Subscript;
class This;
class Times;

class BoolType;
class IDType;
class IntArrayType;
class IntType;
class StringType;

class ClassDecl;
class Ident;
class MainClass;
class MethodDecl;
class Program;
class VarDecl;

class Visitor {
public:
  virtual void visit (Assign *s) = 0;
  virtual void visit (Block *s) = 0;
  virtual void visit (For *s) = 0;
  virtual void visit (IfThenElse *s) = 0;
  virtual void visit (Print *s) = 0;
  virtual void visit (Sidef *s) = 0;
  virtual void visit (While *s) = 0;

  virtual void visit (And *e) = 0;
  virtual void visit (BoolLiteral *e) = 0;
  virtual void visit (Division *e) = 0;
  virtual void visit (Equals *e) = 0;
  virtual void visit (IntLiteral *e) = 0;
  virtual void visit (Length *e) = 0;
  virtual void visit (LessThan *e) = 0;
  virtual void visit (MethodCall *e) = 0;
  virtual void visit (Minus *e) = 0;
  virtual void visit (New *e) = 0;
  virtual void visit (NewIntArray *e) = 0;
  virtual void visit (Not *e) = 0;
  virtual void visit (Or *e) = 0;
  virtual void visit (Plus *e) = 0;
  virtual void visit (StringLiteral *e) = 0;
  virtual void visit (Subscript *e) = 0;
  virtual void visit (This *e) = 0;
  virtual void visit (Times *e) = 0;

  virtual void visit (BoolType *t) = 0;
  virtual void visit (IDType *t) = 0;
  virtual void visit (IntArrayType *t) = 0;
  virtual void visit (IntType *t) = 0;
  virtual void visit (StringType *t) = 0;

  virtual void visit (ClassDecl *n) = 0;
  virtual void visit (Ident *n) = 0;
  virtual void visit (MainClass *n) = 0;
  virtual void visit (MethodDecl *n) = 0;
  virtual void visit (Program *n) = 0;
  virtual void visit (VarDecl *n) = 0;
};

#endif
