#ifndef METHOD_DECL_H
#define METHOD_DECL_H

#include <string>

#include "Tree.h"
#include "type/Type.h"
#include "VarDecl.h"
#include "stmt/Statement.h"
#include "expr/Expression.h"
#include "expr/Ident.h"

using namespace std;

class MethodDecl : public Tree {  
public:
  Type *returnType;
  Ident *methodName;
  vector<VarDecl*> params;
  vector<VarDecl*> varDecls;
  vector<Statement*> body;
  Expression *returnExpr;
  
  MethodDecl () {}
  
  ACCEPT_VISITOR
};

#endif
