/*
 * Description: - Lexer for eMJ (extended Mini Java)
 */

%option noyywrap
%option yylineno

%{
  #include <string.h>
  #include <stdbool.h>
  #include "tokens.h"

  /* initialize column to 1 */
  int column = 1;
  int yycolno = 1;
  bool eof = false;
  bool peekEof = false;
  Token yytok;
  Token peek;
  bool hasLookahead = false;
  Token lookahead;
  
  /* boolean to keep track of multiline comments */
  bool inComment = false;
  // the output file to write to
  FILE *outfp;
  
  /* printf macro to only print when outside of a comment block */
  #define PRINT_TOKEN(f_, ...) if(!inComment) fprintf(outfp, (f_), ##__VA_ARGS__)

  /*
   * Function to maintain column count
   */
  void count();
  
  bool processToken (TokenType type, const std::string& data = "");
%}

INPUTCHARACTER   [^\r\n]
LINE_CMT         "//"{INPUTCHARACTER}*
PRINTLN          System\.out\.println
ID               [a-zA-Z][a-zA-Z0-9_]*
INTLIT           [0-9]+
STRINGLIT        \"[^\"]*\"
COMMA            ,
WHITESPACE       [ \t\f]
LINETERMINATOR   \r|\n|\r\n

%%

<<EOF>>           { peekEof = true; return processToken (TK_EOF); }

"/*"              { count(); inComment = true; }
"*/"              { inComment = false; count(); }
{LINE_CMT}        { count(); }

":"               { if (processToken (TK_COLON)) { return 1; } }
";"               { if (processToken (TK_SEMICOLON)) { return 1; } }
"."               { if (processToken (TK_DOT)) { return 1; } }
","               { if (processToken (TK_COMMA)) { return 1; } }
"="               { if (processToken (TK_EQ)) { return 1; } }
"=="              { if (processToken (TK_EQEQ)) { return 1; } }
"!"               { if (processToken (TK_BANG)) { return 1; } }
"("               { if (processToken (TK_LPAREN)) { return 1; } }
")"               { if (processToken (TK_RPAREN)) { return 1; } }
"["               { if (processToken (TK_LBRACK)) { return 1; } }
"]"               { if (processToken (TK_RBRACK)) { return 1; } }
"{"               { if (processToken (TK_LBRACE)) { return 1; } }
"}"               { if (processToken (TK_RBRACE)) { return 1; } }
"&&"              { if (processToken (TK_AND)) { return 1; } }
"||"              { if (processToken (TK_OR)) { return 1; } }
"<"               { if (processToken (TK_LESSTHAN)) { return 1; } }
"+"               { if (processToken (TK_PLUS)) { return 1; } }
"-"               { if (processToken (TK_MINUS)) { return 1; } }
"*"               { if (processToken (TK_TIMES)) { return 1; } }
"/"               { if (processToken (TK_DIV)) { return 1; } }

"class"           { if (processToken (TK_CLASS)) { return 1; } }
"public"          { if (processToken (TK_PUBLIC)) { return 1; } }
"static"          { if (processToken (TK_STATIC)) { return 1; } }
"void"            { if (processToken (TK_VOID)) { return 1; } }
"String"          { if (processToken (TK_STRING)) { return 1; } }
"extends"         { if (processToken (TK_EXTENDS)) { return 1; } }
"int"             { if (processToken (TK_INT)) { return 1; } }
"boolean"         { if (processToken (TK_BOOLEAN)) { return 1; } }
"while"           { if (processToken (TK_WHILE)) { return 1; } }
"if"              { if (processToken (TK_IF)) { return 1; } }
"else"            { if (processToken (TK_ELSE)) { return 1; } }
"main"            { if (processToken (TK_MAIN)) { return 1; } }
"return"          { if (processToken (TK_RETURN)) { return 1; } }
"length"          { if (processToken (TK_LENGTH)) { return 1; } }
"true"            { if (processToken (TK_TRUE)) { return 1; } }
"false"           { if (processToken (TK_FALSE)) { return 1; } }
"this"            { if (processToken (TK_THIS)) { return 1; } }
"new"             { if (processToken (TK_NEW)) { return 1; } }
{PRINTLN}         { if (processToken (TK_PRINTLN)) { return 1; } }
"sidef"           { if (processToken (TK_SIDEF)) { return 1; } }

{ID}              { if (processToken (TK_ID, yytext)) { return 1; } }
{INTLIT}          { if (processToken (TK_INTLIT, yytext)) { return 1; } }
{STRINGLIT}       {
  char *content = (char*)calloc(strlen(yytext)-1, sizeof(char));
  strncpy(content, &yytext[1], strlen(yytext)-2);
  if (processToken (TK_STRINGLIT, content)) { return 1; }
}

{WHITESPACE}      {/* Ignore whitespace */ count();}
{LINETERMINATOR}  {count();}

.                 { if (processToken (TK_BAD)) { return 1; } }

%%

bool processToken (TokenType type, const std::string& data /*= ""*/) {
  count ();
  if (inComment) {
    return false;
  }
  yytok = (Token){ .type=type, .data=data, .line=yylineno, .column=yycolno };
  return true;
}

void count() {
  yycolno = column;
  for (int i = 0; yytext[i] != '\0'; i++) {
    /* Check for newline */
    if (yytext[i] == '\n') {
      column = 1;
    }
    /* Check for tab */
    else if (yytext[i] == '\t') {
      /* Tab defaults to 4 spaces, increment in blocks of 4 
       * column starts at 1
       */
      column += 4 - ((column-1) % 4);
    }
    /* Otherwise just increment by 1 */
    else {
      column++;
    }
  }
}

void lexBegin (FILE *fp) {
  yyin = fp;
  yylex ();
}

bool lexHasNext () {
  return !eof;
}

Token lexGetNext () {
  if (hasLookahead) {
    Token temp = peek;
    yytok = lookahead;
    hasLookahead = false;
    return temp;
  } else {
    if (peekEof) {
      eof = true;
    }
    Token temp = yytok;
    yylex ();
    return temp;
  }
}

Token lexPeekNext () {
  return hasLookahead ? peek : yytok;
}

Token lexLookahead () {
  if (!hasLookahead) {
    peek = lexGetNext ();
    lookahead = yytok;
    hasLookahead = true;
  }
  return lookahead;
}
