#ifndef TYPE_CHECKER_H
#define TYPE_CHECKER_H

#include <map>
#include <set>
#include <string>
#include <iostream>
#include <fstream>
#include <functional>

#include "../ast/Visitor.h"
#include "../ast/Tree.h"

using namespace std;

class TType;
class Environment;
class Class;
class BinaryOp;
class UnaryOp;
class MethodDecl;

class NameAnalysis : public Visitor {  
private:
  Environment* m_pEnvironment;
  int m_mangleID;
  bool m_isValid;
  bool m_isClassField;
  Class *m_pCurrentClass;
  fstream m_input;
  string m_filename;
  string m_basename;
  int m_errorCount;
  
public:
  NameAnalysis ();
  
public:
  bool analyze (Tree *pAst, const string& filename);
  Environment* environment () { return m_pEnvironment; }
  int errorCount () { return m_errorCount; }
  
private:
  void printError (const string& message, int line, int column);
  void readLine (int targetLine, std::function<void(const string&)> handler);
  
private:
  void visitBinaryOperator (std::string op, BinaryOp *expr);
  void visitUnaryOperator (std::string op, UnaryOp *expr);
  
public:
  void visit (Assign *s);
  void visit (Block *s);
  void visit (For *s);
  void visit (IfThenElse *s);
  void visit (Print *s);
  void visit (Sidef *s);
  void visit (While *s);

  void visit (And *e);
  void visit (BoolLiteral *e);
  void visit (Division *e);
  void visit (Equals *e);
  void visit (IntLiteral *e);
  void visit (Length *e);
  void visit (LessThan *e);
  void visit (MethodCall *e);
  void visit (Minus *e);
  void visit (New *e);
  void visit (NewIntArray *e);
  void visit (Not *e);
  void visit (Or *e);
  void visit (Plus *e);
  void visit (StringLiteral *e);
  void visit (Subscript *e);
  void visit (This *e);
  void visit (Times *e);
  void visit (Ident *e);

  void visit (BoolType *t);
  void visit (IDType *t);
  void visit (IntArrayType *t);
  void visit (IntType *t);
  void visit (StringType *t);

  void visit (ClassDecl *n);
  void visit (MainClass *n);
  void visit (MethodDecl *n);
  void visit (Program *n);
  void visit (VarDecl *n);
};

#endif
