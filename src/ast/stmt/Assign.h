#ifndef ASSIGN_H
#define ASSIGN_H

#include <string>

#include "../expr/Expression.h"
#include "../expr/Ident.h"
#include "SingleStatement.h"

class Assign : public SingleStatement {
public:
  Ident* varID;
  Expression* expr;
  Expression* subscript;
  
  Assign(const std::string& varID, Expression* expr, Expression* subscript=nullptr):
  varID (new Ident (varID)),
  expr (expr),
  subscript (subscript)
  {}
  
  ACCEPT_VISITOR
};

#endif
