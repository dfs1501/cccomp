#ifndef PROGRAM_H
#define PROGRAM_H

#include <vector>

#include "Tree.h"
#include "MainClass.h"
#include "ClassDecl.h"

using namespace std;

class Program : public Tree {
public:
  MainClass* mainClass;
  vector<ClassDecl*> classDecls;

  Program (MainClass *mainClass, const vector<ClassDecl*>& classDecls):
  mainClass (mainClass),
  classDecls (classDecls)
  {}
  
  ACCEPT_VISITOR
};

#endif
