#ifndef FOR_H
#define FOR_H

#include "../expr/Expression.h"
#include "SingleStatement.h"
#include "Statement.h"

class For : public Statement {
public:
  SingleStatement* init;
  Expression*      expr;
  SingleStatement* step;
  Statement*       body;

  For(SingleStatement* init, Expression* expr, SingleStatement* step, Statement* body) {
    this->init = init;
    this->expr = expr;
    this->step = step;
    this->body = body;
  }
  
  ACCEPT_VISITOR
};
#endif