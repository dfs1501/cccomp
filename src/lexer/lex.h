#ifndef LEX_H
#define LEX_H

#include "tokens.h"

// Opens a file for lexing
void lexBegin (FILE *fp);

// Returns true if there are more tokens to be read
bool lexHasNext ();

// Reads the next token from input
Token lexGetNext ();

// Looks at the next token without reading another.
// This is not equivalent to a lookahead, but rather it makes it easier
// to split up the parsing logic into different methods without having
// to keep track of and pass along the last token we read.
// For example:
//   In a method body we can have zero or more variable declarations followed
//   by zero or more method declarations. If we read in an identifier, it could
//   either be the type of the next variable declaration, or the variable on the
//   left-hand side of an assignment. We need to lookahead to the next token
//   to determine which path to take, then call the appropriate parse*() method.
//   If we read in that first identifier then peeked to lookahead, we would
//   have to tell the parse*() method that we already ate its first token.
//   By having an extra peek method, it simplifies not having to implement
//   extra special cases for the parse*() methods.
Token lexPeekNext ();

// Looks ahead to the next token.
Token lexLookahead ();

#endif // LEX_H
