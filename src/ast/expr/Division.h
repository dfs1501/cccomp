#ifndef DIVISION_H
#define DIVISION_H

#include "BinaryOp.h"

class Division : public BinaryOp {
public:
  using BinaryOp::BinaryOp;
  
  ACCEPT_VISITOR
};

#endif