#ifndef LESSTHAN_H
#define LESSTHAN_H

#include "BinaryOp.h"

class LessThan : public BinaryOp {
public:
  using BinaryOp::BinaryOp;
  
  ACCEPT_VISITOR
};

#endif