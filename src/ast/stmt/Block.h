#ifndef BLOCK_H
#define BLOCK_H

#include <vector>

#include "Statement.h"

class Block : public Statement {
public:
  std::vector<Statement*> body;
  
  Block () {}
  
  Block (const std::vector<Statement*>& body):
  body (body)
  {}
  
  ACCEPT_VISITOR
};

#endif
