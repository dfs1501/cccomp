#include "TypeChecker.h"

#include <sstream>

using namespace std;

// TODO: Constructors are not allow to have any arguments

TType * TypeChecker::T_UNASSIGNED = new TUnassigned ();
TType * TypeChecker::T_VOID = new TVoid ();
TType * TypeChecker::T_INT = new TInt ();
TType * TypeChecker::T_STRING = new TString ();
TType * TypeChecker::T_BOOLEAN = new TBoolean ();
TType * TypeChecker::T_INT_ARRAY = new TIntArray ();


TypeChecker::TypeChecker ():
m_isValid (true),
m_pType (T_VOID),
m_errorCount (0)
{}

bool TypeChecker::check (Environment* pEnv, const string& filename) {
  m_errorCount = 0;
  m_filename = filename;
  m_basename = filename.substr (filename.find_last_of ('/')+1);
  m_input = fstream (m_filename);
  
  m_isValid = true;
  
  for (auto p : pEnv->classes) {
    if (p.second->pMainClass) {
    } else {
      typeForMangledName [p.second->pAst->className->mangledName] = new TClass (p.second);
    }
  }
  
  for (auto p : pEnv->classes) {
    Class *pClass = p.second;
    
    if (pClass->pMainClass) {
    } else {
      for (auto var : pClass->pAst->varDecls) {
        var->accept (this);
      }
      
      for (auto m : pClass->pEnvironment->methods) {
        Method *pMethod = m.second;
        m_pEnvironment = pMethod->pEnvironment;
        
        pMethod->pAst->returnType->accept (this);
        //cout << m_pType->name () << " " << pClass->name << "::" << pMethod->name << "()" << endl;
        pMethod->pReturnType = m_pType;
        
        for (auto var : pMethod->pAst->params) {
          var->accept (this);
        }
      }
    }
  }
  
  // Type check each of the classes
  for (auto p : pEnv->classes) {
    Class *pClass = p.second;
    m_pCurrentClass = pClass;
    m_pEnvironment = pClass->pEnvironment;
    
    if (pClass->pMainClass) {
      Method *pMethod = pClass->pEnvironment->methods.begin ()->second;
      m_pEnvironment = pMethod->pEnvironment;
      
      for (auto statement : pMethod->pAst->body) {
        statement->accept (this);
      }
    } else {
      // Type check the class methods
      for (auto m : pClass->pEnvironment->methods) {
        Method *pMethod = m.second;
        m_pEnvironment = pMethod->pEnvironment;
        
        for (auto var : pMethod->pAst->varDecls) {
          var->accept (this);
        }
        
        // Type check the statements in the body of each method
        for (auto statement : pMethod->pAst->body) {
          statement->accept (this);
        }
        
        if (pMethod->pAst->returnExpr) {
          pMethod->pAst->returnExpr->accept (this);
          if (m_pType != T_UNASSIGNED) {
            if (!m_pType->isSubTypeOf (pMethod->pReturnType)) {
              printError ("method '" + pMethod->name + "' expected return value of type '" + pMethod->pReturnType->name () + "'; '" + m_pType->name () + "' given", pMethod->pAst->returnExpr->token.line, pMethod->pAst->returnExpr->token.column);
            }
          }
        }
      }
    }
  }
  
  return m_isValid;
}

void TypeChecker::readLine(int targetLine, std::function<void(const string&)> handler) {
  // We are going to be reading from the input file, so seek back to the
  // beginning of the file.
  m_input.clear();
  m_input.seekg(0, ios::beg);
  
  int lineNum = 0;
  string line;
  
  while (getline(m_input, line)) {
    if (lineNum == targetLine) {
      handler(line);
      break;
    }
    ++lineNum;
  }
}

void TypeChecker::printError (const string& message, int line, int column) {
  m_isValid = false;
  m_pType = T_UNASSIGNED;
  cout << "\033[0;33m" << line << ":" << column << ":" << m_basename << "\033[1;31m error: \033[0m" << message << endl;
  
  ++m_errorCount;
  
  readLine (line-1, [column](const string& line) {
    string noTabs = line;
    string::size_type n = 0;
    string s = "\t";
    string t = "    ";
    while ((n = noTabs.find (s, n)) != string::npos) {
      noTabs.replace (n, s.size (), t);
      n += t.size();
    }
    
    cout << "    " << noTabs << endl;
    cout << "    ";
    cout << string(column-1, ' ');
    cout << '^';
    //cout << string(sourceLoc.col.end - sourceLoc.col.start, '~');
    cout << endl;
  });
}

void TypeChecker::printBinaryOpError (const string& op, TType *pLhs, TType *pRhs, int line, int column) {
  if (pLhs == pRhs) {
    printError ("binary operator '" + op + "' cannot be applied to two '" + pLhs->name () + "' operands", line, column);
  } else {
    printError ("binary operator '" + op + "' cannot be applied to operands of type '" + pLhs->name () + "' and '" + pRhs->name () + "'", line, column);
  }
}

void TypeChecker::visit (Assign *s) {
  // TODO: Make sure this exists
  /*Variable *pVar = m_pEnvironment->findVar (s->varID->name);
  s->expr->accept (this);
  
  if (!m_pType->isSubTypeOf (pVar->pType)) {
    printError ("Type '" + pVar->pType->name () + "' cannot be assigned value of type '" + m_pType->name () + "'", s->token.line, s->token.column);
  }
  
  m_pType = T_VOID;*/
  
  TType *pVarType = typeForMangledName [s->varID->mangledName];
  if (pVarType) {
    s->varID->pType = pVarType;
    if (s->subscript && pVarType != T_UNASSIGNED) {
      if (pVarType != T_INT_ARRAY) {
        printError ("Cannot subscript non-array type '" + pVarType->name () + "'", s->token.line, s->token.column);
        pVarType = T_UNASSIGNED;
      } else {
        pVarType = T_INT;
      }
      s->subscript->accept (this);
      if (m_pType != T_INT && m_pType != T_UNASSIGNED) {
        printError ("Subscript must be of type 'int', '" + m_pType->name () + "' given", s->subscript->token.line, s->subscript->token.column);
      }
    }
    
    s->expr->accept (this);
    
    // Already got error, don't propagate
    if (m_pType != T_UNASSIGNED && pVarType != T_UNASSIGNED) {
      if (!m_pType->isSubTypeOf (pVarType)) {
        if (s->subscript) {
          printError ("array element of type '" + pVarType->name () + "' cannot be assigned value of type '" + m_pType->name () + "'", s->token.line, s->token.column);
        } else {
          printError ("variable '" + s->varID->name + "' of type '" + pVarType->name () + "' cannot be assigned value of type '" + m_pType->name () + "'", s->token.line, s->token.column);
        }
      }
    }
  }
  
  m_pType = T_VOID;
}
void TypeChecker::visit (Block *s) {
  for (Statement *pS : s->body) {
    pS->accept (this);
  }
  m_pType = T_VOID;
}
void TypeChecker::visit (For *s) {
  s->init->accept (this);
  s->expr->accept (this);
  
  if (m_pType != T_BOOLEAN) {
    printError ("For condition must be of boolean type, '" + m_pType->name () + "' given", s->token.line, s->token.column);
  }
  
  s->step->accept (this);
  s->body->accept (this);
  
  m_pType = T_VOID;
}
void TypeChecker::visit (IfThenElse *s) {
  s->expr->accept (this);
  
  if (m_pType != T_BOOLEAN && m_pType != T_UNASSIGNED) {
    printError ("If condition must be of boolean type, '" + m_pType->name () + "' given", s->token.line, s->token.column);
  }
  
  s->then->accept (this);
  if (s->elze) {
    s->elze->accept (this);
  }
  
  m_pType = T_VOID;
}
void TypeChecker::visit (Print *s) {
  s->expr->accept (this);
  
  if (m_pType != T_INT && m_pType != T_BOOLEAN && m_pType != T_STRING && m_pType != T_UNASSIGNED) {
    printError ("Cannot print value of type '" + m_pType->name () + "'", s->token.line, s->token.column);
  }
  
  m_pType = T_VOID;
}
void TypeChecker::visit (Sidef *s) {
  s->expr->accept (this);
  m_pType = T_VOID;
}
void TypeChecker::visit (While *s) {
  s->expr->accept (this);
  
  if (m_pType != T_BOOLEAN && m_pType != T_UNASSIGNED) {
    printError ("While condition must be of boolean type, '" + m_pType->name () + "' given", s->token.line, s->token.column);
  }
  
  s->body->accept (this);
  m_pType = T_VOID;
}

void TypeChecker::visit (And *e) {
  e->lhs->accept (this);
  if (m_pType == T_UNASSIGNED) {
    // We already got an error, so don't keep propagating
    return;
  }
  TType *pLhs = m_pType;
  
  e->rhs->accept (this);
  if (m_pType == T_UNASSIGNED) {
    // We already got an error, so don't keep propagating
    return;
  }
  TType *pRhs = m_pType;
  
  if (pLhs == T_BOOLEAN && pRhs == T_BOOLEAN) {
    m_pType = T_BOOLEAN;
  } else {
    printBinaryOpError ("&&", pLhs, pRhs, e->token.line, e->token.column);
    return;
  }
  
  m_pType = T_BOOLEAN;
  e->pType = m_pType;
}

void TypeChecker::visit (BoolLiteral *e) {
  m_pType = T_BOOLEAN;
  e->pType = m_pType;
}

void TypeChecker::visit (Division *e) {
  e->lhs->accept (this);
  if (m_pType == T_UNASSIGNED) {
    // We already got an error, so don't keep propagating
    return;
  }
  TType *pLhs = m_pType;
  
  e->rhs->accept (this);
  if (m_pType == T_UNASSIGNED) {
    // We already got an error, so don't keep propagating
    return;
  }
  TType *pRhs = m_pType;
  
  if (pLhs == T_INT && pRhs == T_INT) {
    m_pType = T_INT;
  } else {
    printBinaryOpError ("/", pLhs, pRhs, e->token.line, e->token.column);
    return;
  }
  
  m_pType = T_INT;
  e->pType = m_pType;
}

void TypeChecker::visit (Equals *e) {
  e->lhs->accept (this);
  if (m_pType == T_UNASSIGNED) {
    // We already got an error, so don't keep propagating
    return;
  }
  TType *pLhs = m_pType;
  
  e->rhs->accept (this);
  if (m_pType == T_UNASSIGNED) {
    // We already got an error, so don't keep propagating
    return;
  }
  TType *pRhs = m_pType;
  
  if ((pLhs->asClassType () && pRhs->asClassType ()) || (pLhs == pRhs)) {
    m_pType = T_BOOLEAN;
  } else {
    printBinaryOpError ("==", pLhs, pRhs, e->token.line, e->token.column);
    return;
  }
  
  m_pType = T_BOOLEAN;
  e->pType = m_pType;
}

void TypeChecker::visit (IntLiteral *e) {
  m_pType = T_INT;
  e->pType = m_pType;
}

void TypeChecker::visit (Length *e) {
  e->expr->accept (this);
  if (m_pType != T_INT_ARRAY && m_pType != T_UNASSIGNED) {
    printError ("Only array types have a length", e->token.line, e->token.column);
  }
  m_pType = T_INT;
  e->pType = m_pType;
}

void TypeChecker::visit (LessThan *e) {
  e->lhs->accept (this);
  if (m_pType == T_UNASSIGNED) {
    // We already got an error, so don't keep propagating
    return;
  }
  TType *pLhs = m_pType;
  
  e->rhs->accept (this);
  if (m_pType == T_UNASSIGNED) {
    // We already got an error, so don't keep propagating
    return;
  }
  TType *pRhs = m_pType;
  
  if (pLhs == T_INT && pRhs == T_INT) {
    m_pType = T_BOOLEAN;
  } else {
    printBinaryOpError ("<", pLhs, pRhs, e->token.line, e->token.column);
    return;
  }
  
  m_pType = T_BOOLEAN;
  e->pType = m_pType;
}

void TypeChecker::visit (MethodCall *e) {
  e->expr->accept (this);
  if (m_pType == T_UNASSIGNED) {
    // We already got an error, so don't keep propagating
    return;
  }
  
  TClass *pClassType = m_pType->asClassType ();
  if (!pClassType) {
    printError ("Method '" + e->methodName->name + "' cannot be called on primitive type '" + m_pType->name () + "'", e->token.line, e->token.column);
    return;
  }
  
  Method *pMethod = pClassType->pClassObj->pEnvironment->findMethod (e->methodName->name);
  if (!pMethod) {
    printError ("Type '" + pClassType->name () + "' does not contain a definition for method '" + e->methodName->name + "'", e->token.line, e->token.column);
    return;
  }
  
  if (pMethod->params.size () != e->params.size ()) {
    stringstream ss;
    ss << "Method '" << e->methodName->name << "' expected " << pMethod->params.size () << " parameter" << (pMethod->params.size () == 1 ? "" : "s") << ", but " << e->params.size () << " " << (e->params.size () == 1 ? "was" : "were") << " given";
    printError (ss.str (), e->token.line, e->token.column);
  }
  
  for (int i = 0; i < min (e->params.size (), pMethod->params.size ()); ++i) {
    e->params [i]->accept (this);
    if (m_pType == T_UNASSIGNED) {
      // We already got an error, so don't keep propagating
      continue;
    }
    TType *pFormal = typeForMangledName [pMethod->params [pMethod->paramOrder [i]]->name + "_" + to_string (pMethod->params [pMethod->paramOrder [i]]->mangleID) + "_"];//pMethod->params [pMethod->paramOrder [i]]->pType;
    if (pFormal) {
      if (!m_pType->isSubTypeOf (pFormal)) {
        stringstream ss;
        ss << "Type mismatch at parameter #" << (i+1) << " of method '" << e->methodName->name << "'; expected '" << pFormal->name () << "', but '" << m_pType->name () << "' was given";
        Token tok = e->params [i]->token;
        printError (ss.str (), tok.line, tok.column);
        continue;
      }
    }
  }
  
  m_pType = pMethod->pReturnType;
  e->pType = m_pType;
  e->pMethod = pMethod;
}

void TypeChecker::visit (Minus *e) {
  e->lhs->accept (this);
  if (m_pType == T_UNASSIGNED) {
    // We already got an error, so don't keep propagating
    return;
  }
  TType *pLhs = m_pType;
  
  e->rhs->accept (this);
  if (m_pType == T_UNASSIGNED) {
    // We already got an error, so don't keep propagating
    return;
  }
  TType *pRhs = m_pType;
  
  if (pLhs == T_INT && pRhs == T_INT) {
    m_pType = T_INT;
  } else {
    printBinaryOpError ("-", pLhs, pRhs, e->token.line, e->token.column);
    return;
  }
  
  m_pType = T_INT;
  e->pType = m_pType;
}

void TypeChecker::visit (New *e) {
  //Class *pClass = m_pEnvironment->findClass (e->ID->name);
  //m_pType = new TClass (pClass);
  //e->pType = m_pType;
  
  TType *pType = typeForMangledName [e->ID->mangledName];
  if (pType) {
    TClass *pClassType = pType->asClassType ();
    if (pClassType) {
      m_pType = pClassType;
      e->pType = m_pType;
    } else {
      printError ("Cannot instantiate non-class type '" + pType->name () + "'", e->token.line, e->token.column);
    }
  } else {
    m_pType = T_UNASSIGNED;
  }
}

void TypeChecker::visit (NewIntArray *e) {
  e->expr->accept (this);
  
  if (m_pType != T_INT && m_pType != T_UNASSIGNED) {
    printError ("Array size expression must be of type 'int'; '" + m_pType->name () + "' given", e->expr->token.line, e->expr->token.column);
  }
  
  m_pType = T_INT_ARRAY;
  e->pType = m_pType;
}

void TypeChecker::visit (Not *e) {
  e->expr->accept (this);
  if (m_pType == T_UNASSIGNED) {
    // We already got an error, so don't keep propagating
    return;
  }
  
  if (m_pType == T_BOOLEAN) {
    m_pType = T_BOOLEAN;
  } else {
    printError ("Unary operator '!' cannot be applied to operand of type '" + m_pType->name () + "'", e->token.line, e->token.column);
    return;
  }
  
  m_pType = T_BOOLEAN;
  e->pType = m_pType;
}

void TypeChecker::visit (Or *e) {
  e->lhs->accept (this);
  if (m_pType == T_UNASSIGNED) {
    // We already got an error, so don't keep propagating
    return;
  }
  TType *pLhs = m_pType;
  
  e->rhs->accept (this);
  if (m_pType == T_UNASSIGNED) {
    // We already got an error, so don't keep propagating
    return;
  }
  TType *pRhs = m_pType;
  
  if (pLhs == T_BOOLEAN && pRhs == T_BOOLEAN) {
    m_pType = T_BOOLEAN;
  } else {
    printBinaryOpError ("||", pLhs, pRhs, e->token.line, e->token.column);
    return;
  }
  
  m_pType = T_BOOLEAN;
  e->pType = m_pType;
}

void TypeChecker::visit (Plus *e) {
  e->lhs->accept (this);
  if (m_pType == T_UNASSIGNED) {
    // We already got an error, so don't keep propagating
    return;
  }
  TType *pLhs = m_pType;
  
  e->rhs->accept (this);
  if (m_pType == T_UNASSIGNED) {
    // We already got an error, so don't keep propagating
    return;
  }
  TType *pRhs = m_pType;
  
  if (pLhs == T_INT && pRhs == T_INT) {
    m_pType = T_INT;
  } else if (
    (pLhs == T_INT || pLhs == T_STRING) && 
    (pRhs == T_INT || pRhs == T_STRING)
  ) {
    m_pType = T_STRING;
  } else {
    printBinaryOpError ("+", pLhs, pRhs, e->token.line, e->token.column);
    return;
  }
  
  e->pType = m_pType;
}

void TypeChecker::visit (StringLiteral *e) {
  m_pType = T_STRING;
  e->pType = m_pType;
}

void TypeChecker::visit (Subscript *e) {
  e->expr->accept (this);
  
  if (m_pType != T_INT_ARRAY && m_pType != T_UNASSIGNED) {
    printError ("Cannot subscript non-array type '" + m_pType->name () + "'", e->token.line, e->token.column);
  }
  
  e->subscript->accept (this);
  
  if (m_pType != T_INT && m_pType != T_UNASSIGNED) {
    printError ("Subscript must be of type 'int'; '" + m_pType->name () + "' given", e->token.line, e->token.column);
  }
  
  m_pType = T_INT;
  e->pType = m_pType;
}

void TypeChecker::visit (This *e) {
  m_pType = new TClass (m_pCurrentClass);
  e->pType = m_pType;
}

void TypeChecker::visit (Times *e) {
  e->lhs->accept (this);
  TType *pLhs = m_pType;
  
  e->rhs->accept (this);
  TType *pRhs = m_pType;
  
  if (pLhs == T_INT && pRhs == T_INT) {
    m_pType = T_INT;
  } else {
    printBinaryOpError ("*", pLhs, pRhs, e->token.line, e->token.column);
    return;
  }
  
  e->pType = m_pType;
}

void TypeChecker::visit (Ident *e) {
  //Variable *pVar = m_pEnvironment->findVar (e->name);
  //m_pType = pVar->pType;
  //e->pType = m_pType;
  
  m_pType = typeForMangledName [e->mangledName];
  if (m_pType) {
    e->pType = m_pType;
  } else {
    m_pType = T_UNASSIGNED;
  }
}


void TypeChecker::visit (BoolType *t) {
  m_pType = T_BOOLEAN;
  t->pType = m_pType;
}
void TypeChecker::visit (IDType *t) {
  // TODO: If this is null, then there's and issue with name analysis
  //Class *pClass = m_pEnvironment->findClass (t->ID->name);
  //m_pType = new TClass (pClass);
  //t->pType = m_pType;
  
  TType *pType = typeForMangledName [t->ID->mangledName];
  if (pType) {
    TClass *pClassType = pType->asClassType ();
    if (pClassType) {
      m_pType = pClassType;
      t->pType = m_pType;
    } else {
      // This should never happen
      printError ("Cannot use non-class type '" + pType->name () + "'", t->token.line, t->token.column);
    }
  } else {
    m_pType = T_UNASSIGNED;
  }
}
void TypeChecker::visit (IntArrayType *t) {
  m_pType = T_INT_ARRAY;
  t->pType = m_pType;
}
void TypeChecker::visit (IntType *t) {
  m_pType = T_INT;
  t->pType = m_pType;
}
void TypeChecker::visit (StringType *t) {
  m_pType = T_STRING;
  t->pType = m_pType;
}

void TypeChecker::visit (ClassDecl *n) {}
void TypeChecker::visit (MainClass *n) {}
void TypeChecker::visit (MethodDecl *n) {}
void TypeChecker::visit (Program *n) {}

void TypeChecker::visit (VarDecl *n) {
  n->type->accept (this);
  n->pType = m_pType;
  
  typeForMangledName [n->name->mangledName] = m_pType;
  n->pType = m_pType;
  
  //Variable *pVar = m_pEnvironment->findVar (n->name->name);
  //pVar->pType = m_pType;
  //n->pType = m_pType;
}
