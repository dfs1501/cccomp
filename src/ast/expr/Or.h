#ifndef OR_H
#define OR_H

#include "BinaryOp.h"

class Or : public BinaryOp {
public:
  using BinaryOp::BinaryOp;
  
  ACCEPT_VISITOR
};

#endif