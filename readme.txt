Dan Stafford
Sarathi Hansen

We have chosen to use C++ for our compiler and have included a make file in the
base directory. To build our source just `cd` into the `compiler` directory and
run `make`.  To run the executable `cd` into the release directory and run emjc
