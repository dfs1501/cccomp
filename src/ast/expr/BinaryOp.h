#ifndef BINARY_OP_H
#define BINARY_OP_H

#include "Expression.h"

class BinaryOp : public Expression {
public:
  Expression* lhs;
  Expression* rhs;
  
  BinaryOp(Expression* lhs, Expression* rhs) {
    this->lhs = lhs;
    this->rhs = rhs;
  }
};

#endif
