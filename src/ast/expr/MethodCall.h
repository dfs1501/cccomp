#ifndef METHOD_CALL_H
#define METHOD_CALL_H

#include <string>
#include <vector>

#include "Expression.h"
#include "Ident.h"

using namespace std;

class MethodCall : public Expression {
public:
  Expression *expr;
  Ident *methodName;
  vector<Expression*> params;
  
  Method *pMethod;
  
  MethodCall (Expression *expr, const string& methodName, const vector<Expression*>& params):
  expr (expr),
  methodName (new Ident (methodName)),
  params (params),
  pMethod (nullptr)
  {}
  
  ACCEPT_VISITOR
};

#endif
