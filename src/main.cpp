#include <iostream>
#include <fstream>
#include <vector>
#include <functional>

#include <stdio.h>

#include "lexer/lex.h"
#include "parser/Parser.h"
#include "ast/Program.h"
#include "tree_printer/TreePrinter.h"
#include "semantics/NameAnalysis.h"
#include "semantics/TypeChecker.h"
#include "code_generator/CodeGenerator.h"

using namespace std;


void readLine(ifstream& input, int targetLine, std::function<void(const string&)> handler) {
  // We are going to be reading from the input file, so seek back to the
  // beginning of the file.
  input.clear();
  input.seekg(0, ios::beg);
  
  int lineNum = 0;
  string line;
  
  while (getline(input, line)) {
    if (lineNum == targetLine) {
      handler(line);
      break;
    }
    ++lineNum;
  }
}

/**
 * Replaces the extension of a filename. If the filename doesn't have an
 * extension, then the new extension is added.
 */
void replaceExt(string& s, const string& newExt) {
  string::size_type i = s.rfind('.', s.length());
  if (i == string::npos) {
    // If we don't have an extension, then just append the new one.
    s += "." + newExt;
  } else {
    s.replace(i+1, newExt.length(), newExt);
  }
}

/**
 * Creates the output file with the ext extension and then opens the file for 
 * writing using the outputFile reference
 */
void createOutputFile (const string& inputFilename, const string& ext, ofstream &outputFile) {
  // Create the output file name: <input_file_name>.ext
  string outputFilename (inputFilename);
  replaceExt (outputFilename, ext);
  
  // Try to open the output file for writing and print an error if we can't
  outputFile.open (outputFilename);
  if (!outputFile) {
    cerr << "Error: cannot open file for writing '" << outputFilename << "'" << endl;
  }
}

/**
 * Usage/help printout
 */
void printHelp () {
  cout << "Usage: emjc [options] <source file>" << endl;
  cout << "  ––help: Prints a synopsis of options" << endl;
  cout << "  ––pp: Pretty-prints the input file to the standard output" << endl;
  cout << "  ––lex: Generates output from lexical analysis" << endl;
  cout << "  ––ast: Generates output from syntactic analysis" << endl;
  cout << "  ––name: Generates output from name analysis" << endl;
  cout << "  ––type: Generates output from type analysis" << endl;
  cout << "  ––cgen: Generates executable Java class files" << endl;
}

Program* doParse (FILE *in, const string& filename) {
  try {
    Parser parser;
    Program *ast = parser.parse (in);
    fclose (in);
    return ast;
  }
  catch (Parser::ParseError err) {
    // <line>:<column> error:<description>
    fclose (in);
    string basename = filename.substr (filename.find_last_of ('/')+1);
    ifstream input (filename);
    
    cout << "\033[0;33m" << err.token.line << ":" << err.token.column << ":" << basename << "\033[1;31m error: \033[0m" << err.message << endl;
    
    readLine (input, err.token.line-1, [err](const string& line) {
      string noTabs = line;
      string::size_type n = 0;
      string s = "\t";
      string t = "    ";
      while ((n = noTabs.find (s, n)) != string::npos) {
        noTabs.replace (n, s.size (), t);
        n += t.size();
      }
      
      cout << "    " << noTabs << endl;
      cout << "    ";
      cout << string(err.token.column-1, ' ');
      cout << '^';
      //cout << string(sourceLoc.col.end - sourceLoc.col.start, '~');
      cout << endl;
    });
    cout << "1 error generated." << endl;
  }
  return nullptr;
}

/**
 * Function to just lex the file.  Writes the lexed tokens to the out
 */
void doLex (FILE *in, ostream& out) {
  lexBegin (in);
  while (lexHasNext ()) {
    Token tok = lexGetNext ();
    out << tok.line << ":" << tok.column << " ";
    out << TokenTypeToString [tok.type] << "(" << tok.data << ")" << endl;
  }
}

/**
 * Function to parse the file.  Writes the parsed ast to out
 */
void doAST (FILE *in, const string& filename, ostream& out) {
  Program *ast = doParse (in, filename);
  if (ast) {
#ifdef __DEBUG
    // If debugging, just print the output to the terminal instead
    TreePrinter *printer = new TreePrinter (cout);
#else
    // Otherwise use the out file
    TreePrinter *printer = new TreePrinter (out);
#endif
    ast->accept (printer);
  }
}

void doNameAnalysis (FILE *in, const string& filename) {
  Program *ast = doParse (in, filename);
  if (ast) {
    NameAnalysis nameAnalysis;
    if (nameAnalysis.analyze (ast, filename)) {
      cout << "Valid eMiniJava Program" << endl;
    } else {
      int errCount = nameAnalysis.errorCount ();
      cout << errCount << " error" << (errCount == 1 ? "" : "s") << " generated." << endl;
    }
  }
}

void doPrettyPrint (FILE *in, const string& filename) {
  Program *ast = doParse (in, filename);
  if (ast) {
    NameAnalysis nameAnalysis;
    if (nameAnalysis.analyze (ast, filename)) {
      TreePrinter *printer = new TreePrinter (cout, true);
      ast->accept (printer);
    } else {
      int errCount = nameAnalysis.errorCount ();
      cout << errCount << " error" << (errCount == 1 ? "" : "s") << " generated." << endl;
    }
  }
}

void doTypeCheck (FILE *in, const string& filename) {
  Program *ast = doParse (in, filename);
  if (ast) {
    NameAnalysis nameAnalysis;
    nameAnalysis.analyze (ast, filename);
    
    TypeChecker typeChecker;
    typeChecker.check (nameAnalysis.environment (), filename);
    int errCount = nameAnalysis.errorCount () + typeChecker.errorCount ();
    if (errCount > 0) {
      cout << errCount << " error" << (errCount == 1 ? "" : "s") << " generated." << endl;
    } else {
      cout << "Valid eMiniJava Program" << endl;
    }
  }
}

void doCodeGeneration (FILE *in, const string& filename) {
  Program *ast = doParse (in, filename);
  if (ast) {
    NameAnalysis nameAnalysis;
    nameAnalysis.analyze (ast, filename);
    
    TypeChecker typeChecker;
    typeChecker.check (nameAnalysis.environment (), filename);
    int errCount = nameAnalysis.errorCount () + typeChecker.errorCount ();
    if (errCount > 0) {
      cout << errCount << " error" << (errCount == 1 ? "" : "s") << " generated." << endl;
    } else {
      std::string fileDir = filename.substr(0, filename.find_last_of('/'));
      CodeGenerator codeGenerator (fileDir, true);
      ast->accept (&codeGenerator);
      string jasminCommand = "java -jar jasmin/jasmin.jar -d " + fileDir;
      for (const string& file : codeGenerator.generatedJasminFiles ()) {
        jasminCommand += " " + file;
      }
      system (jasminCommand.c_str ());
    }
  }
}

/**
 * Main function
 * argv[1] should contain an option (--lex|--ast)
 * argv[2] should contain the name of the output file
 */
int main (int argc, char **argv) {
  // If we don't have any command line arguments,
  // or the user used the --help option,
  // or the option isn't help, but we don't have a file,
  // then print the help message
  if (argc == 1 || string(argv[1]) == "--help" || argc != 3) {
    printHelp ();
    return 0;
  }

  string option = argv[1];
  string filename = argv[2];
  
  if (option != "--lex" &&
      option != "--ast" &&
      option != "--pp" &&
      option != "--name" &&
      option != "--type" &&
      option != "--cgen")
  {
    printHelp ();
    return 0;
  }
  
  // Try to open the input file for reading and print an error if we can't
  FILE *in = fopen(filename.data(), "r");
  if (!in) {
    cerr << "Error: cannot open file for reading '" << filename << "'" << endl;
    return 1;
  }
  
  ofstream out;
  // If the option given was --lex, then call the lexer
  if (option == "--lex") {
    createOutputFile (filename, "lexed", out);
    if (out) {
      doLex (in, out);
    }
  }
  // If the option given was --ast, then call the parser
  else if (option == "--ast") {
    createOutputFile (filename, "ast", out);
    if (out) {
      doAST (in, filename, out);
    }
  }
  // If the option given was --name, then call the name analysis
  else if (option == "--name") {
    doNameAnalysis (in, filename);
  }
  // If the option given was --pp, then call the pretty printer
  else if (option == "--pp") {
    doPrettyPrint (in, filename);
  }
  else if (option == "--type") {
    doTypeCheck (in, filename);
  }
  else if (option == "--cgen") {
    doCodeGeneration (in, filename);
  }
  // Unrecognized switch or no parameter was given, print out the usage
  else {
    printHelp ();
  }
  
  return 0;
}
