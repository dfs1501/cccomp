#ifndef TType_h
#define TType_h

#include <string>
#include <iostream>

using namespace std;

#include "Environment.h"

class TUnassigned;
class TVoid;
class TInt;
class TIntArray;
class TBoolean;
class TString;
class TClass;

class TType {
public:
  virtual string name () = 0;
  
  virtual TClass* asClassType () { return nullptr; }
  
  virtual bool isSubTypeOf (TType *pType) { return pType->isSubTypeOfDD (this); }
  virtual bool isSubTypeOfDD (TType *pType) = 0;
  
  virtual bool isSubTypeOfDD (TUnassigned *pUnassigned) { return false; }
  virtual bool isSubTypeOfDD (TVoid *pVoid) { return false; }
  virtual bool isSubTypeOfDD (TInt *pInt) { return false; }
  virtual bool isSubTypeOfDD (TIntArray *pIntArray) { return false; }
  virtual bool isSubTypeOfDD (TBoolean *pBoolean) { return false; }
  virtual bool isSubTypeOfDD (TString *pString) { return false; }
  virtual bool isSubTypeOfDD (TClass *pClass) { return false; }
};

class TUnassigned : public TType {
  string name () override { return "unassigned"; }
  
  bool isSubTypeOfDD (TType *pType) override { return pType->isSubTypeOfDD (this); }
};

class TVoid : public TType {
  string name () override { return "void"; }
  
  bool isSubTypeOfDD (TType *pType) override { return pType->isSubTypeOfDD (this); }
  
  bool isSubTypeOfDD (TVoid *pVoid) override { return true; }
};

class TInt : public TType {
  string name () override { return "int"; }
  
  bool isSubTypeOfDD (TType *pType) override { return pType->isSubTypeOfDD (this); }
  
  bool isSubTypeOfDD (TInt *pInt) override { return true; }
};

class TIntArray : public TType {
  string name () override { return "int[]"; }
  
  bool isSubTypeOfDD (TType *pType) override { return pType->isSubTypeOfDD (this); }
  
  bool isSubTypeOfDD (TIntArray *pIntArray) override { return true; }
};

class TBoolean : public TType {
  string name () override { return "boolean"; }
  
  bool isSubTypeOfDD (TType *pType) override { return pType->isSubTypeOfDD (this); }
  
  bool isSubTypeOfDD (TBoolean *pBoolean) override { return true; }
};

class TString : public TType {
  string name () override { return "String"; }
  
  bool isSubTypeOfDD (TType *pType) override { return pType->isSubTypeOfDD (this); }
  
  bool isSubTypeOfDD (TString *pString) override { return true; }
};

class TClass : public TType {
public:
  Class *pClassObj;
  
public:
  TClass (Class *pClass): pClassObj (pClass) {}
  
public:
  string name () override { return pClassObj->name; }
  
  TClass* asClassType () override { return this; }
  
  bool isSubTypeOfDD (TType *pType) override { return pType->isSubTypeOfDD (this); }
  
  bool isSubTypeOfDD (TClass *pClass) override {
    if (pClassObj->name == pClass->pClassObj->name) {
      return true;
    }
    if (pClassObj->pBaseClass) {
      Class *pBase = pClass->pClassObj;
      while (pBase) {
        if (pClassObj->pBaseClass == pBase) {
          return true;
        }
        pBase = pBase->pBaseClass;
      }
    }
    return (pClass->pClassObj->name == "Object");
  }
};

#endif // TType_h
