#ifndef Environment_H
#define Environment_H

#include <string>
#include <map>
#include <vector>

#include "../ast/Tree.h"

using namespace std;


class TType;
class TClass;

struct Variable;
struct Method;
struct Class;

struct Environment {
  Environment *pParent;
  map<string, string> mangledNames;
  map<string, Variable*> vars;
  map<string, Method*> methods;
  map<string, Class*> classes;
  
  Class *pCurrentClass;
  Method *pCurrentMethod;
  
  Environment ():
    pParent (nullptr),
    pCurrentMethod (nullptr),
    pCurrentClass (nullptr)
  {}
  
  Variable* findVar (const string& name, bool localOnly=false);
  Method* findMethod (const string& name, bool localOnly=false);
  Class* findClass (const string& name, bool localOnly=false);
  
  Class* tryDeclareClass (const string& name, Environment *pEnvironment, int mangleID);
  Method* tryDeclareMethod (const string& name, Environment *pEnvironment, int mangleID);
  Variable* tryDeclareVariable (const string& name, Class *pClass, int mangleID);
  
  Environment* enterScope () {
    Environment *pNew = new Environment ();
    pNew->pParent = this;
    return pNew;
  }
  
  Environment* leaveScope () {
    return pParent;
  }
};

struct Variable {
  string name;
  int mangleID;
  TType *pType;
  VarDecl *pAst;
  Class *pClass;
  
  Variable ();
};

struct Method {
  string name;
  int mangleID;
  Environment *pEnvironment;
  MethodDecl *pAst;
  TType *pReturnType;
  map<string, Variable*> params;
  vector <string> paramOrder;
  
  Method ();
};

struct Class {
  string name;
  int mangleID;
  TType *pType;
  Environment *pEnvironment;
  MainClass *pMainClass;
  ClassDecl *pAst;
  Class *pBaseClass;
  
  Class ();
};

#endif
