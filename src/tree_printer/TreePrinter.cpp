#include "TreePrinter.h"

#include "../ast/Ast.h"
#include "../semantics/TType.h"

TreePrinter::TreePrinter(std::ostream& out, bool pretty/*= false*/):
m_out (out),
m_indent (0),
m_pretty (pretty)
{}

void TreePrinter::nextLine () {
  m_out << std::endl;
  for (int i = 0; i < m_indent; ++i) {
    m_out << m_indentToken;
  }
}
void TreePrinter::indentNextLine () {
  ++m_indent;
  nextLine ();
}
void TreePrinter::unindentNextLine () {
  --m_indent;
  nextLine ();
}


// Expressions
void TreePrinter::visit(IntLiteral* e) {
  m_out << e->value;
}
void TreePrinter::visit(StringLiteral* e) {
  m_out << "\"" << e->value << "\"";
}
void TreePrinter::visit (BoolLiteral *e) {
  m_out << (e->value ? "true" : "false");
}

void TreePrinter::visit (Length *e) {
  if (m_pretty) {
    e->expr->accept (this);
    m_out << ".length ()";
  } else {
    m_out << "(LENGTH ";
    e->expr->accept (this);
    m_out << ")";
  }
}
void TreePrinter::visit (MethodCall *e) {
  if (m_pretty) {
    e->expr->accept (this);
    m_out << ".";
    e->methodName->accept (this);
    m_out << " (";
    bool first = true;
    for (auto expr : e->params) {
      if (!first) {
        first = true;
        m_out << ", ";
      }
      expr->accept (this);
    }
    m_out << ")";
  } else {
    m_out << "((. ";
    e->expr->accept (this);
    m_out << " ";
    e->methodName->accept (this);
    m_out << ")";
    for (auto expr : e->params) {
      m_out << " ";
      expr->accept (this);
    }
    m_out << ")";
  }
}

void TreePrinter::visit (New *e) {
  if (m_pretty) {
    m_out << "new ";
    e->ID->accept (this);
    m_out << " ()";
  } else {
    m_out << "(NEW ";
    e->ID->accept (this);
    m_out << ")";
  }
}
void TreePrinter::visit (NewIntArray *e) {
  if (m_pretty) {
    m_out << "new int[]";
  } else {
    m_out << "(NEW int[] ";
    e->expr->accept (this);
    m_out << ")";
  }
}

void TreePrinter::visit (Subscript *e) {
  if (m_pretty) {
    e->expr->accept (this);
    m_out << " [";
    e->subscript->accept (this);
    m_out << "]";
  } else {
    m_out << "(SUBSCRIPT ";
    e->expr->accept (this);
    m_out << " ";
    e->subscript->accept (this);
    m_out << ")";
  }
}
void TreePrinter::visit (This *e) {
  m_out << "this";
}

// Binary Operators
void TreePrinter::visitBinaryOperator (std::string op, BinaryOp *expr) {
  if (m_pretty) {
    expr->lhs->accept(this);
    m_out << " " + op + " ";
    expr->rhs->accept(this);
  } else {
    m_out << "(" << op << " ";
    expr->lhs->accept(this);
    m_out << " ";
    expr->rhs->accept(this);
    m_out << ")";
  }
}
void TreePrinter::visit(Plus* e) {
  visitBinaryOperator ("+", e);
}
void TreePrinter::visit(Minus* e) {
  visitBinaryOperator ("-", e);
}
void TreePrinter::visit(Times* e) {
  visitBinaryOperator ("*", e);
}
void TreePrinter::visit(Division* e) {
  visitBinaryOperator ("/", e);
}
void TreePrinter::visit(Equals* e) {
  visitBinaryOperator ("==", e);
}
void TreePrinter::visit(LessThan* e) {
  visitBinaryOperator ("<", e);
}
void TreePrinter::visit(And* e) {
  visitBinaryOperator ("&&", e);
}
void TreePrinter::visit(Or* e) {
  visitBinaryOperator ("||", e);
}

// Unary Operators
void TreePrinter::visitUnaryOperator (std::string op, UnaryOp *expr) {
  if (m_pretty) {
    m_out << op;
    expr->expr->accept (this);
  } else {
    m_out << "(" << op << " ";
    expr->expr->accept(this);
    m_out << ")";
  }
}
void TreePrinter::visit(Not* e) {
  visitUnaryOperator ("!", e);
}


// Statements
void TreePrinter::visit(IfThenElse* s) {
  if (m_pretty) {
    m_out << "if (";
    s->expr->accept (this);
    m_out << ") {";
    
    indentNextLine ();
    s->then->accept (this);
    
    --m_indent;
    nextLine ();
    m_out << "}";
    
    if (s->elze) {
      m_out << " else {";
      
      indentNextLine ();
      s->elze->accept (this);
      
      --m_indent;
      nextLine ();
      m_out << "}";
    }
  } else {
    m_out << "(IF ";
    s->expr->accept (this);
    
    indentNextLine ();
    s->then->accept (this);
    
    if (s->elze) {
      nextLine ();
      s->elze->accept (this);
    }
    m_out << ")";
    --m_indent;
  }
}

void TreePrinter::visit(Print* s) {
  if (m_pretty) {
    m_out << "System.out.println (";
    s->expr->accept (this);
    m_out << ");";
  } else {
    m_out << "(PRINTLN ";
    s->expr->accept (this);
    m_out << ")";
  }
}

void TreePrinter::visit(Assign* s) {
  if (m_pretty) {
    s->varID->accept (this);
    m_out << " = ";
    s->expr->accept (this);
    m_out << ";";
  } else {
    m_out << "(= ";
    s->varID->accept (this);
    m_out << " ";
    s->expr->accept (this);
    m_out << ")";
  }
}

void TreePrinter::visit(While* s) {
  if (m_pretty) {
    m_out << "while (";
    s->expr->accept (this);
    m_out << ") {";
    
    indentNextLine ();
    s->body->accept (this);
    
    --m_indent;
    nextLine ();
    m_out << "}";
  } else {
    m_out << "(WHILE ";
    s->expr->accept (this);
    
    indentNextLine ();
    s->body->accept (this);
    m_out << ")";
    --m_indent;
  }
}

void TreePrinter::visit(For* s) {
  m_out << "(FOR ";
  s->init->accept (this);
  m_out << " ";
  s->expr->accept (this);
  m_out << " ";
  s->step->accept (this);
  
  indentNextLine ();
  s->body->accept (this);
  m_out << ")";
  --m_indent;
}

void TreePrinter::visit(Block* block) {
  if (m_pretty) {
    m_out << "{";
    ++m_indent;
    for (auto s : block->body) {
      nextLine ();
      s->accept (this);
    }
    --m_indent;
    nextLine ();
    m_out << "}";
  } else {
    m_out << "(BLOCK";
    ++m_indent;
    for (auto s : block->body) {
      nextLine ();
      s->accept (this);
    }
    m_out << ")";
    --m_indent;
  }
}

void TreePrinter::visit (Sidef *s) {
  if (m_pretty) {
    m_out << "sidef (";
    s->expr->accept (this);
    m_out << ");";
  } else {
    m_out << "(SIDEF ";
    s->expr->accept (this);
    m_out << ")";
  }
}


// Types
void TreePrinter::visit (BoolType *t) {
  m_out << "boolean";
}
void TreePrinter::visit (IDType *t) {
  if (m_pretty) {
    t->ID->accept (this);
  } else {
    m_out << "(TY-ID ";
    t->ID->accept (this);
    m_out << ")";
  }
}
void TreePrinter::visit (IntArrayType *t) {
  m_out << "int[]";
}
void TreePrinter::visit (IntType *t) {
  m_out << "int";
}
void TreePrinter::visit (StringType *t) {
  m_out << "String";
}


// Other

void TreePrinter::visit (ClassDecl *n) {
  if (m_pretty) {
    m_out << "class ";
    n->className->accept (this);
    
    if (n->baseClassName->name != "") {
      m_out << " extends ";
      n->baseClassName->accept (this);
    }
    
    m_out << " {";
    
    ++m_indent;
    for (auto decl : n->varDecls) {
      nextLine ();
      decl->accept (this);
    }
    
    for (auto decl : n->methodDecls) {
      nextLine ();
      decl->accept (this);
    }
    
    --m_indent;
    nextLine ();
    m_out << "}";
  } else {
    m_out << "(CLASS ";
    n->className->accept (this);
    
    if (n->baseClassName) {
      m_out << " ";
      n->baseClassName->accept (this);
    }
    
    ++m_indent;
    for (auto decl : n->varDecls) {
      nextLine ();
      decl->accept (this);
    }
    
    for (auto decl : n->methodDecls) {
      nextLine ();
      decl->accept (this);
    }
    
    m_out << ")";
    --m_indent;
}
}
void TreePrinter::visit (Ident *n) {
  if (m_pretty) {
    m_out << n->mangledName;
  } else {
    m_out << "(ID " << n->name << ")";
  }
}
void TreePrinter::visit (MainClass *n) {
  if (m_pretty) {
    m_out << "class ";
    n->className->accept (this);
    m_out << " {";
    indentNextLine ();
    m_isMainClass = true;
    n->mainMethod->accept (this);
    m_isMainClass = false;
    --m_indent;
    nextLine ();
    m_out << "}";
  } else {
    m_out << "(MAIN-CLASS ";
    n->className->accept (this);
    
    indentNextLine ();
    n->mainMethod->accept (this);
    m_out << ")";
    --m_indent;
  }
}
void TreePrinter::visit (MethodDecl *n) {
  if (m_pretty) {
    m_out << "public ";
    n->returnType->accept (this);
    m_out << " ";
    n->methodName->accept (this);
    m_out << " (";
    bool first = true;
    for (auto param : n->params) {
      if (! first) {
        first = true;
        m_out << ", ";
      }
      if (m_isMainClass) {
        m_out << "String[]";
      } else {
        param->type->accept (this);
      }
      m_out << " ";
      param->name->accept (this);
    }
    m_out << ") {";
    
    ++m_indent;
    for (auto decl : n->varDecls) {
      nextLine ();
      decl->accept (this);
    }
    
    for (auto stmt : n->body) {
      nextLine ();
      stmt->accept (this);
    }
    
    // Main method doesn't have return
    if (n->returnExpr) {
      nextLine ();
      m_out << "return ";
      n->returnExpr->accept (this);
      m_out << ";";
    }
    
    --m_indent;
    nextLine ();
    m_out << "}";
  } else {
    m_out << "(MTD-DECL ";
    n->returnType->accept (this);
    m_out << " ";
    n->methodName->accept (this);
    m_out << " (TY-ID-LIST";
    for (auto param : n->params) {
      m_out << " (";
      if (m_isMainClass) {
        m_out << "String[]";
      } else {
        param->type->accept (this);
      }
      m_out << " ";
      param->name->accept (this);
      m_out << ")";
    }
    m_out << ")";
    
    ++m_indent;
    for (auto decl : n->varDecls) {
      nextLine ();
      decl->accept (this);
    }
    
    for (auto stmt : n->body) {
      nextLine ();
      stmt->accept (this);
    }
    
    // Main method doesn't have return
    if (n->returnExpr) {
      nextLine ();
      m_out << "(RETURN ";
      n->returnExpr->accept (this);
      m_out << ")";
    }
    
    m_out << ")";
    --m_indent;
  }
}
void TreePrinter::visit (Program *n) {
  n->mainClass->accept (this);
  for (auto decl : n->classDecls) {
    nextLine ();
    nextLine ();
    decl->accept (this);
  }
  m_out << endl;
}
void TreePrinter::visit (VarDecl *n) {
  if (m_pretty) {
    n->type->accept (this);
    m_out << " ";
    n->name->accept (this);
    m_out << ";";
  } else {
    m_out << "(VAR-DECL ";
    n->type->accept (this);
    m_out << " ";
    n->name->accept (this);
    m_out << ")";
  }
}
