#ifndef NEW_INT_ARRAY_H
#define NEW_INT_ARRAY_H

#include "Expression.h"

using namespace std;

class NewIntArray : public Expression {
public:
  Expression *expr;
  
  NewIntArray (Expression *expr):
  expr (expr)
  {}
  
  ACCEPT_VISITOR
};

#endif
