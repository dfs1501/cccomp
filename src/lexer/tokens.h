#ifndef TOKENS_H
#define TOKENS_H

#include <string>
#include <map>

enum TokenType {
  TK_BAD,
  TK_EOF,
  
  TK_COLON,
  TK_SEMICOLON,
  TK_DOT,
  TK_COMMA,
  TK_EQ,
  TK_EQEQ,
  TK_BANG,
  TK_LPAREN,
  TK_RPAREN,
  TK_LBRACK,
  TK_RBRACK,
  TK_LBRACE,
  TK_RBRACE,
  TK_AND,
  TK_OR,
  TK_LESSTHAN,
  TK_PLUS,
  TK_MINUS,
  TK_TIMES,
  TK_DIV,
  
  TK_CLASS,
  TK_PUBLIC,
  TK_STATIC,
  TK_VOID,
  TK_STRING,
  TK_EXTENDS,
  TK_INT,
  TK_BOOLEAN,
  TK_WHILE,
  TK_IF,
  TK_ELSE,
  TK_MAIN,
  TK_RETURN,
  TK_LENGTH,
  TK_TRUE,
  TK_FALSE,
  TK_THIS,
  TK_NEW,
  TK_PRINTLN,
  TK_SIDEF,
  
  TK_ID,
  TK_INTLIT,
  TK_STRINGLIT,
};

typedef struct Token {
  TokenType type;
  std::string data;
  int line;
  int column;
} Token;

extern std::map<TokenType, std::string> TokenTypeToString;

#endif // TOKENS_H
