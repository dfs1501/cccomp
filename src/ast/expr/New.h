#ifndef NEW_H
#define NEW_H

#include <string>

#include "Expression.h"
#include "Ident.h"

using namespace std;

class New : public Expression {
public:
  Ident *ID;
  
  New (string ID):
  ID (new Ident (ID))
  {}
  
  ACCEPT_VISITOR
};

#endif