#ifndef EQUALS_H
#define EQUALS_H

#include "BinaryOp.h"

class Equals : public BinaryOp {
public:
  using BinaryOp::BinaryOp;
  
  ACCEPT_VISITOR
};

#endif