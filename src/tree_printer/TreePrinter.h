#ifndef TREEPRINTER_H
#define TREEPRINTER_H

#include <iostream>
#include <string>

#include "../ast/Visitor.h"

class BinaryOp;
class UnaryOp;

class TreePrinter : public Visitor {
private:
  std::ostream& m_out;
  int m_indent;
  const std::string m_indentToken = "  ";
  bool m_pretty;
  bool m_isMainClass;
  
private:
  void nextLine ();
  void indentNextLine ();
  void unindentNextLine ();
  
private:
  void visitBinaryOperator (std::string op, BinaryOp *expr);
  void visitUnaryOperator (std::string op, UnaryOp *expr);

public:
  TreePrinter(std::ostream& out, bool pretty=false);
  
  void visit (Assign *s);
  void visit (Block *s);
  void visit (For *s);
  void visit (IfThenElse *s);
  void visit (Print *s);
  void visit (Sidef *s);
  void visit (While *s);

  void visit (And *e);
  void visit (BoolLiteral *e);
  void visit (Division *e);
  void visit (Equals *e);
  void visit (IntLiteral *e);
  void visit (Length *e);
  void visit (LessThan *e);
  void visit (MethodCall *e);
  void visit (Minus *e);
  void visit (New *e);
  void visit (NewIntArray *e);
  void visit (Not *e);
  void visit (Or *e);
  void visit (Plus *e);
  void visit (StringLiteral *e);
  void visit (Subscript *e);
  void visit (This *e);
  void visit (Times *e);

  void visit (BoolType *t);
  void visit (IDType *t);
  void visit (IntArrayType *t);
  void visit (IntType *t);
  void visit (StringType *t);

  void visit (ClassDecl *n);
  void visit (Ident *n);
  void visit (MainClass *n);
  void visit (MethodDecl *n);
  void visit (Program *n);
  void visit (VarDecl *n);
};

#endif
