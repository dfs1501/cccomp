#include "CodeGenerator.h"

#include "../ast/Ast.h"
#include "../semantics/TType.h"
// TODO: Move type definitions so we don't need to include this
#include "../semantics/TypeChecker.h"
#include "../semantics/Environment.h"


string CodeGenerator::printType (TType *pType) {
  if (pType == TypeChecker::T_VOID) {
    return "V";
  } else if (pType == TypeChecker::T_INT) {
    return "I";
  } else if (pType == TypeChecker::T_STRING) {
    return "Ljava/lang/String;";
  } else if (pType == TypeChecker::T_BOOLEAN) {
    return "I";
  } else if (pType == TypeChecker::T_INT_ARRAY) {
    return "[I";
  } else {
    TClass *pClassType = pType->asClassType ();
    if (pClassType) {
      return "L" + pClassType->name () + ";";
    } else {
      return "MISSING_TYPE";
    }
  }
}

string CodeGenerator::printLocalType (TType *pType) {
  if (pType == TypeChecker::T_INT) {
    return "i";
  } else if (pType == TypeChecker::T_STRING) {
    return "a";
  } else if (pType == TypeChecker::T_BOOLEAN) {
    return "i";
  } else if (pType == TypeChecker::T_INT_ARRAY) {
    return "a";
  } else {
    if (pType->asClassType ()) {
      return "a";
    } else {
      return "MISSING_TYPE";
    }
  }
}


// Expressions
void CodeGenerator::visit(IntLiteral* e) {
  (*m_pOut) << "\tldc " << e->value << endl;
}
void CodeGenerator::visit(StringLiteral* e) {
  (*m_pOut) << "\tldc " << '"' << e->value << '"' << endl;
}
void CodeGenerator::visit (BoolLiteral *e) {
  (*m_pOut) << "\ticonst_" << (e->value ? 1 : 0) << endl;
}

void CodeGenerator::visit (Length *e) {
  e->expr->accept (this);
  (*m_pOut) << "\tarraylength" << endl;
}
void CodeGenerator::visit (MethodCall *e) {
  e->expr->accept (this);
  for (auto p : e->params) {
    p->accept (this);
  }
  (*m_pOut) << "\tinvokevirtual " << e->expr->pType->name () << "/" << e->methodName->name << "(";
  for (auto p : e->pMethod->pAst->params) {
    (*m_pOut) << printType (p->pType);
  }
  (*m_pOut) << ")";
  (*m_pOut) << printType (e->pType);
  (*m_pOut) << endl;
}

void CodeGenerator::visit (New *e) {
  (*m_pOut) << "\tnew " << e->ID->name << endl;
  (*m_pOut) << "\tdup" << endl;
  (*m_pOut) << "\tinvokespecial " << e->ID->name << "/<init>()V" << endl;
}
void CodeGenerator::visit (NewIntArray *e) {
  e->expr->accept (this);
  (*m_pOut) << "\tnewarray " << "int" << endl;
}

void CodeGenerator::visit (Subscript *e) {
  e->expr->accept (this);
  e->subscript->accept (this);
  (*m_pOut) << "\tiaload" << endl;
}

void CodeGenerator::visit (This *e) {
  (*m_pOut) << "\taload 0" << endl;
}

// Binary Operators
void CodeGenerator::visit(Plus* e) {
  if (e->lhs->pType == TypeChecker::T_INT &&
      e->rhs->pType == TypeChecker::T_INT)
  {
    e->lhs->accept (this);
    e->rhs->accept (this);
    (*m_pOut) << "\tiadd" << endl;
  } else {
    (*m_pOut) << "\tnew java/lang/StringBuilder" << endl;
    (*m_pOut) << "\tdup" << endl;
    (*m_pOut) << "\tinvokespecial java/lang/StringBuilder/<init>()V" << endl;
    e->lhs->accept (this);
    (*m_pOut) << "\tinvokevirtual java/lang/StringBuilder/append(" << printType (e->lhs->pType) << ")Ljava/lang/StringBuilder;" << endl;
    e->rhs->accept (this);
    (*m_pOut) << "\tinvokevirtual java/lang/StringBuilder/append(" << printType (e->rhs->pType) << ")Ljava/lang/StringBuilder;" << endl;
    (*m_pOut) << "\tinvokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;" << endl;
  }
}
void CodeGenerator::visit(Minus* e) {
  e->lhs->accept (this);
  e->rhs->accept (this);
  (*m_pOut) << "\tisub" << endl;
}
void CodeGenerator::visit(Times* e) {
  e->lhs->accept (this);
  e->rhs->accept (this);
  (*m_pOut) << "\timul" << endl;
}
void CodeGenerator::visit(Division* e) {
  e->lhs->accept (this);
  e->rhs->accept (this);
  (*m_pOut) << "\tidiv" << endl;
}
void CodeGenerator::visit(Equals* e) {
  int ilabel = m_ilabel;
  ++m_ilabel;
  
  e->lhs->accept (this);
  e->rhs->accept (this);
  
  string neLabel = "ne_" + to_string (ilabel);
  string afterLabel = "after_" + to_string (ilabel);
  
  // type checker would have made sure lhs and rhs are same type, so just use type of lhs
  (*m_pOut) << "\tif_" << printLocalType (e->lhs->pType) << "cmpne " << neLabel << endl;
  (*m_pOut) << "\ticonst_1" << endl;
  (*m_pOut) << "\tgoto " << afterLabel << endl;
  (*m_pOut) << neLabel << ":" << endl;
  (*m_pOut) << "\ticonst_0" << endl;
  (*m_pOut) << afterLabel << ":" << endl;
}
void CodeGenerator::visit(LessThan* e) {
  int ilabel = m_ilabel;
  ++m_ilabel;
  
  e->lhs->accept (this);
  e->rhs->accept (this);
  
  string geLabel = "ge_" + to_string (ilabel);
  string afterLabel = "after_" + to_string (ilabel);
  
  (*m_pOut) << "\tif_icmpge " << geLabel << endl;
  (*m_pOut) << "\ticonst_1" << endl;
  (*m_pOut) << "\tgoto " << afterLabel << endl;
  (*m_pOut) << geLabel << ":" << endl;
  (*m_pOut) << "\ticonst_0" << endl;
  (*m_pOut) << afterLabel << ":" << endl;
}
void CodeGenerator::visit(And* e) {
  int ilabel = m_ilabel;
  ++m_ilabel;
  
  e->lhs->accept (this);
  (*m_pOut) << "\ticonst_0" << endl;
  (*m_pOut) << "\tif_icmpeq And_Initial_False_" << ilabel << endl;
  e->rhs->accept (this);
  (*m_pOut) << "\tgoto And_Initial_True_" << ilabel << endl;
  (*m_pOut) << "And_Initial_False_" << ilabel << ":" << endl;
  (*m_pOut) << "\ticonst_0" << endl;
  (*m_pOut) << "And_Initial_True_" << ilabel << ":" << endl;
}
void CodeGenerator::visit(Or* e) {
  int ilabel = m_ilabel;
  ++m_ilabel;
  
  e->lhs->accept (this);
  (*m_pOut) << "\ticonst_0" << endl;
  (*m_pOut) << "\tif_icmpne Or_Initial_True_" << ilabel << endl;
  e->rhs->accept (this);
  (*m_pOut) << "\tgoto Or_Initial_False_" << ilabel << endl;
  (*m_pOut) << "Or_Initial_True_" << ilabel << ":" << endl;
  (*m_pOut) << "\ticonst_1" << endl;
  (*m_pOut) << "Or_Initial_False_" << ilabel << ":" << endl;
}

// Unary Operators
void CodeGenerator::visit(Not* e) {
  (*m_pOut) << "\ticonst_1" << endl;
  e->expr->accept (this);
  (*m_pOut) << "\tisub" << endl;
}


// Statements
void CodeGenerator::visit(IfThenElse* s) {
  string elseLabel = "If_Else_" + to_string (m_ilabel);
  string endLabel = "If_End_" + to_string (m_ilabel);
  m_ilabel += 1;
  
  s->expr->accept (this);
  (*m_pOut) << "\ticonst_0" << endl;
  (*m_pOut) << "\tif_icmpeq " << elseLabel << endl;
  s->then->accept (this);
  (*m_pOut) << "\tgoto " << endLabel << endl;
  (*m_pOut) << elseLabel << ":" << endl;
  if (s->elze) {
    s->elze->accept (this);
  }
  (*m_pOut) << endLabel << ":" << endl;
}

void CodeGenerator::visit(Print* s) {
  (*m_pOut) << "\tgetstatic java/lang/System/out Ljava/io/PrintStream;" << endl;
  s->expr->accept (this);
  if (s->expr->pType == TypeChecker::T_BOOLEAN) {
    string elseLabel = "If_Else_" + to_string (m_ilabel);
    string endLabel = "If_End_" + to_string (m_ilabel);
    m_ilabel += 1;
    
    (*m_pOut) << "\ticonst_0" << endl;
    (*m_pOut) << "\tif_icmpeq " << elseLabel << endl;
    (*m_pOut) << "\tldc \"true\"" << endl;
    (*m_pOut) << "\tgoto " << endLabel << endl;
    (*m_pOut) << elseLabel << ":" << endl;
    (*m_pOut) << "\tldc \"false\"" << endl;
    (*m_pOut) << endLabel << ":" << endl;
    
    (*m_pOut) << "\tinvokevirtual java/io/PrintStream/println(Ljava/lang/String;)V" << endl;
  } else {
    (*m_pOut) << "\tinvokevirtual java/io/PrintStream/println(" << printType (s->expr->pType) << ")V" << endl;
  }
}

void CodeGenerator::visit(Assign* s) {
  if (s->subscript) {
    s->varID->accept (this);
    s->subscript->accept (this);
    s->expr->accept (this);
    (*m_pOut) << "\t" << printLocalType (s->expr->pType) << "astore" << endl;
  } else {
    if (s->varID->pClass) {
      (*m_pOut) << "\taload 0" << endl;
      s->expr->accept (this);
      (*m_pOut) << "\tputfield " << s->varID->pClass->name << "/" << s->varID->name << " " << printType (s->expr->pType) << endl;
    } else {
      s->expr->accept (this);
      (*m_pOut) << "\t" << printLocalType (s->expr->pType) << "store " << m_locals [s->varID->mangledName] << endl;
    }
  }
}

void CodeGenerator::visit(While* s) {
  string exprLabel = "whileExpr_" + to_string (m_ilabel);
  string bodyLabel = "whileBody_" + to_string (m_ilabel);
  m_ilabel += 1;

  // Faster to only have one jump in the actual while loop
  (*m_pOut) << "\tgoto " << exprLabel << endl;
  (*m_pOut) << bodyLabel << ":" << endl;
  s->body->accept (this);
  (*m_pOut) << exprLabel << ":" << endl;
  s->expr->accept (this);
  // Push 0 to compare agains the expression
  (*m_pOut) << "\ticonst_0" << endl;
  // Exits while loop by not jumping
  (*m_pOut) << "\tif_icmpne " << bodyLabel << endl;
}

void CodeGenerator::visit(For* s) {
  // We don't actually have for loops in the language
  /*string exprLabel = "forExpr_" + to_string (m_ilabel);
  string bodyLabel = "forBody_" + to_string (m_ilabel);

  s->init->accept (this);
  // Faster to only have one jump in the actual for loop
  (*m_pOut) << "\tgoto " << exprLabel << endl;
  (*m_pOut) << bodyLabel << ":" << endl;
  s->body->accept (this);
  s->step->accept (this);
  (*m_pOut) << exprLabel << ":" << endl;
  s->expr->accept (this);
  // Push 0 to compare agains the expression
  (*m_pOut) << "\ticonst_0" << endl;
  // Exits for loop by not jumping
  (*m_pOut) << "\tif_icmpne " << bodyLabel << endl;*/
}

void CodeGenerator::visit(Block* block) {
  for (auto stmt : block->body) {
    stmt->accept (this);
  }
}

void CodeGenerator::visit (Sidef *s) {
  s->expr->accept (this);
  (*m_pOut) << "\tpop" << endl;
}


// Types
void CodeGenerator::visit (BoolType *t) {}
void CodeGenerator::visit (IDType *t) {}
void CodeGenerator::visit (IntArrayType *t) {}
void CodeGenerator::visit (IntType *t) {}
void CodeGenerator::visit (StringType *t) {}


// Other

void CodeGenerator::visit (Program *n) {
  n->mainClass->accept (this);
  for (ClassDecl *c : n->classDecls) {
    c->accept (this);
  }
}


void CodeGenerator::visit (MainClass *n) {
  delete m_pOut;
  m_pOut = new ofstream (m_dir + "/" + n->className->name + ".j");
  
  m_generatedJasminFiles.push_back (m_dir + "/" + n->className->name + ".j");
  
  (*m_pOut) << ".class public " << n->className->name << endl;
  (*m_pOut) << ".super java/lang/Object" << endl;
  
  m_inMainClass = true;
  m_pMethodDecl = nullptr;
  
  n->mainMethod->accept (this);
}


void CodeGenerator::visit (ClassDecl *n) {
  delete m_pOut;
  m_pOut = new ofstream (m_dir + "/" + n->className->name + ".j");
  
  m_generatedJasminFiles.push_back (m_dir + "/" + n->className->name + ".j");
  
  m_inMainClass = false;
  m_pClassDecl = n;
  m_pMethodDecl = nullptr;
  
  (*m_pOut) << ".class public " << n->className->name << endl;
  
  if (n->baseClassName->name != "") {
    (*m_pOut) << ".super " << n->baseClassName->name << endl;
  } else {
    (*m_pOut) << ".super java/lang/Object" << endl;
  }
  
  for (auto v : n->varDecls) {
    (*m_pOut) << ".field private " << v->name->name << " " << printType (v->pType) << endl;
  }
  
  MethodDecl *pConstructor = nullptr;
  for (auto m : n->methodDecls) {
    if (m->methodName->name == n->className->name) {
      pConstructor = m;
      break;
    }
  }
  
  // TODO: Abstract this logic to the visit methoddecl function?
  (*m_pOut) << endl << endl;
  (*m_pOut) << ".method public <init>()V" << endl;
  (*m_pOut) << "\t.limit locals 2" << endl;
  (*m_pOut) << "\t.limit stack 100" << endl;
  (*m_pOut) << "\taload 0" << endl;
  if (n->baseClassName->name != "") {
    (*m_pOut) << "\tinvokespecial " << n->baseClassName->name << "/<init>()V";
  } else {
    (*m_pOut) << "\tinvokespecial java/lang/Object/<init>()V";
  }
  (*m_pOut) << endl;
  
  if (pConstructor) {
    // Reference to 'this' is a local, so +1
    int localsCount = pConstructor->varDecls.size () + 1;
    (*m_pOut) << "\t.limit locals " << localsCount << endl;
    // TODO: Calculate minimum required stack size?
    (*m_pOut) << "\t.limit stack " << 100 << endl;
    
    m_locals.clear ();
    m_ilocal = 1;
    for (auto v : pConstructor->varDecls) {
      v->accept (this);
    }
    
    for (auto s : pConstructor->body) {
      s->accept (this);
    }
  }
  
  // TODO: In type checker, prevent constructor from returning a value
  (*m_pOut) << "\treturn" << endl;
  (*m_pOut) << ".end method" << endl;
  
  for (auto m : n->methodDecls) {
    if (m == pConstructor) {
      continue;
    }
    m->accept (this);
  }
}


void CodeGenerator::visit (MethodDecl *n) {
  (*m_pOut) << endl << endl;
  if (m_inMainClass) {
    (*m_pOut) << ".method public static main([Ljava/lang/String;)V";
  } else {
    (*m_pOut) << ".method public " << n->methodName->name << "(";
    for (auto p : n->params) {
      (*m_pOut) << printType (p->pType);
    }
    (*m_pOut) << ")" << printType (n->returnExpr->pType);
  }
  (*m_pOut) << endl;
  
  int localsCount = n->varDecls.size () + n->params.size ();
  if (!m_inMainClass) {
    // reference to 'this' is a local, unless we're static (main)
    localsCount += 1;
  }
  (*m_pOut) << "\t.limit locals " << localsCount << endl;
  // TODO: Calculate minimum required stack size?
  (*m_pOut) << "\t.limit stack " << 100 << endl;
  
  m_locals.clear ();
  m_ilocal = 1;
  
  if (!m_inMainClass) {
    for (auto p : n->params) {
      m_locals [p->name->mangledName] = m_ilocal;
      ++m_ilocal;
    }
  }
  
  for (auto v : n->varDecls) {
    v->accept (this);
  }
  
  for (auto s : n->body) {
    s->accept (this);
  }
  
  if (n->returnExpr) {
    n->returnExpr->accept (this);
    (*m_pOut) << "\t" << printLocalType (n->returnExpr->pType) << "return" << endl;
  } else {
    (*m_pOut) << "\treturn" << endl;
  }
  
  (*m_pOut) << ".end method" << endl;
}


void CodeGenerator::visit (VarDecl *n) {
  // This isn't called for class fields
  m_locals [n->name->mangledName] = m_ilocal;
  ++m_ilocal;
}

void CodeGenerator::visit (Ident *n) {
  if (n->pClass) {
    (*m_pOut) << "\taload 0" << endl;
    (*m_pOut) << "\tgetfield " << n->pClass->name << "/" << n->name << " " << printType (n->pType) << endl;
  } else {
    (*m_pOut) << "\t" << printLocalType (n->pType) << "load " << m_locals [n->mangledName] << endl;
  }
}
