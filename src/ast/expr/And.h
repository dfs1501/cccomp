#ifndef AND_H
#define AND_H

#include "BinaryOp.h"

class And : public BinaryOp {
public:
  using BinaryOp::BinaryOp;
  
  ACCEPT_VISITOR
};

#endif
