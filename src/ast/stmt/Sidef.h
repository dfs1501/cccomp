#ifndef SIDEF_H
#define SIDEF_H

#include "SingleStatement.h"
#include "../expr/Expression.h"

class Sidef : public SingleStatement {
public:
  Expression *expr;
  
  Sidef(Expression *expr):
  expr (expr)
  {}
  
  ACCEPT_VISITOR
};

#endif
