#ifndef VAR_DECL_H
#define VAR_DECL_H

#include <string>

#include "expr/Ident.h"
#include "Tree.h"
#include "type/Type.h"

using namespace std;

class VarDecl : public Tree {
public:
  Type *type;
  Ident *name;
  
  VarDecl (Type *type, const string& name):
  type (type),
  name(new Ident(name))
  {}
  
  ACCEPT_VISITOR
};

#endif
