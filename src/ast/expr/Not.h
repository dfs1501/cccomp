#ifndef NOT_H
#define NOT_H

#include "UnaryOp.h"

class Not : public UnaryOp {
public:
  using UnaryOp::UnaryOp;
  
  ACCEPT_VISITOR
};

#endif