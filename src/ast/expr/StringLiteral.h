#ifndef STRINGLITERAL_H
#define STRINGLITERAL_H

#include <string>

#include "Expression.h"

class StringLiteral : public Expression {
public:
  std::string value;
  
  StringLiteral(std::string value) {
    this->value = value;
  }

  ACCEPT_VISITOR
};

#endif