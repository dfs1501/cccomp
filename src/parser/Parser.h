#ifndef PARSER_H
#define PARSER_H

#include <set>
#include <string>
#include <vector>
#include <functional>

#include "../lexer/tokens.h"
#include "../lexer/lex.h"

using namespace std;

class Program;
class MainClass;
class ClassDecl;
class VarDecl;
class MethodDecl;
class Type;
class Statement;
class Expression;

/**
 * An LL(1) parser that calls the appropriate lex methods to take tokens as
 * input and generate a resulting AST for the extended mini java language.
 */
class Parser {
public:
  /**
   * A structure that encapsulates the token at which a parse error occured
   * and a message with any extra information about the error.
   */
  struct ParseError {
    Token token;
    string message;
    
    ParseError (const Token& token, const string& message):
    token (token),
    message (message)
    {}
  };
  
public:
  Parser () {}
  
private:
  // Reads a token from input and throws an exception if it is not of the given
  // 'expected' token type.
  Token expect (
    TokenType expected,
    string errorMessage = "",
    string file = "",
    int line = -1);
  
  // Runs a parsing function zero or more times until the given follow token is
  // seen. If 'consume' is true, then the follow token will be consumed from
  // the token input stream.
  template <typename T>
  vector<T> zeroOrMore (function<T()> f, set<TokenType> followSet, bool consume = true) {
    vector<T> nodes;
    do {
      if (!lexHasNext ()) {
        // Throw error
        std::string errorMessage = "unexpected end of file";
#ifdef __DEBUG
        errorMessage += "<" + std::string(__FILE__) + ":" + to_string (__LINE__) + ">";
#endif
        throw ParseError (lexPeekNext (), errorMessage);
        break;
      }
      if (followSet.find (lexPeekNext ().type) != followSet.end ()) {
        if (consume) {
          lexGetNext ();
        }
        break;
      }
      nodes.push_back (f ());
    } while (true);
    return nodes;
  }
  
public:
  // Parses the given input file.
  Program* parse (FILE *in);
  
private:
  // One method for each variable in the grammar
  Program* parseProgram ();
  MainClass* parseMainClass ();
  ClassDecl* parseClassDeclaration ();
  VarDecl* parseVarDeclaration ();
  MethodDecl* parseMethodDeclaration ();
  Type* parseType ();
  Statement* parseStatement ();
  
  Expression* parseExpression ();
  Expression* parseOrExp ();
  Expression* parseAndExp ();
  Expression* parseCompExp ();
  Expression* parsePlusExp ();
  Expression* parseTimesExp ();
  Expression* parseNotExp ();
  Expression* parsePostfixExp ();
  Expression* parseAtom ();
};

#endif // PARSER_H
