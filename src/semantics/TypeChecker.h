#ifndef TYPECHECKER_H
#define TYPECHECKER_H

#include <map>
#include <set>
#include <string>
#include <iostream>
#include <fstream>
#include <functional>

#include "../ast/Visitor.h"
#include "../ast/Ast.h"
#include "Environment.h"
#include "TType.h"

using namespace std;

class TypeChecker : public Visitor {
public:
  static TType *T_UNASSIGNED;
  static TType *T_VOID;
  static TType *T_INT;
  static TType *T_STRING;
  static TType *T_BOOLEAN;
  static TType *T_INT_ARRAY;
  
private:
  TType *m_pType;
  Environment *m_pEnvironment;
  Class *m_pCurrentClass;
  map<string, TType*> typeForMangledName;
  bool m_isValid;
  fstream m_input;
  string m_filename;
  string m_basename;
  int m_errorCount;
public:
  TypeChecker ();
  bool check (Environment* pEnv, const string& filename);
  int errorCount () { return m_errorCount; }
private:
  void printError (const string& message, int line, int column);
  void printBinaryOpError (const string& op, TType *pLhs, TType *pRhs, int line, int column);
  void readLine (int targetLine, std::function<void(const string&)> handler);
  
public:
  void visit (Assign *s);
  void visit (Block *s);
  void visit (For *s);
  void visit (IfThenElse *s);
  void visit (Print *s);
  void visit (Sidef *s);
  void visit (While *s);

  void visit (And *e);
  void visit (BoolLiteral *e);
  void visit (Division *e);
  void visit (Equals *e);
  void visit (IntLiteral *e);
  void visit (Length *e);
  void visit (LessThan *e);
  void visit (MethodCall *e);
  void visit (Minus *e);
  void visit (New *e);
  void visit (NewIntArray *e);
  void visit (Not *e);
  void visit (Or *e);
  void visit (Plus *e);
  void visit (StringLiteral *e);
  void visit (Subscript *e);
  void visit (This *e);
  void visit (Times *e);
  void visit (Ident *e);

  void visit (BoolType *t);
  void visit (IDType *t);
  void visit (IntArrayType *t);
  void visit (IntType *t);
  void visit (StringType *t);

  void visit (ClassDecl *n);
  void visit (MainClass *n);
  void visit (MethodDecl *n);
  void visit (Program *n);
  void visit (VarDecl *n);
};

#endif