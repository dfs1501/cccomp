#ifndef CODEGENERATOR_H
#define CODEGENERATOR_H

#include <iostream>
#include <string>

#include "../ast/Visitor.h"
#include "../semantics/TType.h"


struct Environment;
struct Class;
struct Method;


class CodeGenerator : public Visitor {
private:
  string m_dir;
  std::ostream *m_pOut;
  int m_ilabel;
  int m_ilocal;
  map<string, int> m_locals;
  bool m_verbose;
  vector<string> m_generatedJasminFiles;
  
  bool m_inMainClass;
  ClassDecl *m_pClassDecl;
  MethodDecl *m_pMethodDecl;

public:
  CodeGenerator (const string& dir, bool verbose=false): m_dir (dir), m_verbose (verbose), m_ilabel (0), m_pOut (nullptr), m_pClassDecl (nullptr), m_pMethodDecl (nullptr) {}
  
  string printType (TType *pType);
  string printLocalType (TType *pType);
  
  const vector<string>& generatedJasminFiles () {
    return m_generatedJasminFiles;
  }
  
  void visit (Assign *s);
  void visit (Block *s);
  void visit (For *s);
  void visit (IfThenElse *s);
  void visit (Print *s);
  void visit (Sidef *s);
  void visit (While *s);

  void visit (And *e);
  void visit (BoolLiteral *e);
  void visit (Division *e);
  void visit (Equals *e);
  void visit (IntLiteral *e);
  void visit (Length *e);
  void visit (LessThan *e);
  void visit (MethodCall *e);
  void visit (Minus *e);
  void visit (New *e);
  void visit (NewIntArray *e);
  void visit (Not *e);
  void visit (Or *e);
  void visit (Plus *e);
  void visit (StringLiteral *e);
  void visit (Subscript *e);
  void visit (This *e);
  void visit (Times *e);

  void visit (BoolType *t);
  void visit (IDType *t);
  void visit (IntArrayType *t);
  void visit (IntType *t);
  void visit (StringType *t);

  void visit (ClassDecl *n);
  void visit (Ident *n);
  void visit (MainClass *n);
  void visit (MethodDecl *n);
  void visit (Program *n);
  void visit (VarDecl *n);
};

#endif
