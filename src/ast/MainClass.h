#ifndef MAIN_CLASS_H
#define MAIN_CLASS_H

#include <string>

#include "Tree.h"

using namespace std;

class MainClass : public Tree {
public:
  Ident *className;
  MethodDecl *mainMethod;

  MainClass () {}
  
  ACCEPT_VISITOR
};

#endif
