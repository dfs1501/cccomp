CC = g++
CFLAGS = -std=c++11

# Debug and Release Directories
REL_DIR = release
DEB_DIR = debug
SRC_DIR = src

EXE = emjc
REL_EXE = $(addprefix $(REL_DIR)/, $(EXE))
DEB_EXE = $(addprefix $(DEB_DIR)/, $(EXE))

# Lex Object and C file lists
LEX_FILES = lexer/lex
LEX_OBJS = $(addsuffix .o, $(LEX_FILES))
REL_LEX_OBJS = $(addprefix $(REL_DIR)/, $(LEX_OBJS))
DEB_LEX_OBJS = $(addprefix $(DEB_DIR)/, $(LEX_OBJS))
LEX_C = $(addprefix src/, $(addsuffix .c, $(LEX_FILES)))

# Object file lists for Debug and Release
OBJS = main.o lexer/tokens.o parser/Parser.o \
	   tree_printer/TreePrinter.o semantics/NameAnalysis.o \
	   semantics/Environment.o semantics/TypeChecker.o \
	   code_generator/CodeGenerator.o
REL_OBJS = $(addprefix $(REL_DIR)/, $(OBJS))
DEB_OBJS = $(addprefix $(DEB_DIR)/, $(OBJS))

.PHONY: release
release: $(REL_EXE)

.PHONY: debug
debug: CFLAGS += -g -D__DEBUG
debug: $(DEB_EXE)

# Main executable
$(DEB_EXE): $(DEB_OBJS) $(DEB_LEX_OBJS)
	mkdir -p $(@D)
	$(CC) $(CFLAGS) -o $@ $^
$(REL_EXE): $(REL_OBJS) $(REL_LEX_OBJS)
	mkdir -p $(@D)
	$(CC) $(CFLAGS) -o $@ $^

# Compile the lexer .c files
$(DEB_LEX_OBJS): $(DEB_DIR)/%.o: $(SRC_DIR)/%.c
	mkdir -p $(@D)
	$(CC) $(CFLAGS) -c -o $@ $<
$(REL_LEX_OBJS): $(REL_DIR)/%.o: $(SRC_DIR)/%.c
	mkdir -p $(@D)
	$(CC) $(CFLAGS) -c -o $@ $<

# Generate the lexer .c files
$(LEX_C): %.c: %.lex
	flex -o $@ $<

# Compile .cpp files
$(DEB_OBJS): $(DEB_DIR)/%.o: $(SRC_DIR)/%.cpp
	mkdir -p $(@D)
	$(CC) $(CFLAGS) -c -o $@ $<
$(REL_OBJS): $(REL_DIR)/%.o: $(SRC_DIR)/%.cpp
	mkdir -p $(@D)
	$(CC) $(CFLAGS) -c -o $@ $<

.PHONY: clean
clean:
	rm -rf $(REL_DIR) $(DEB_DIR) $(LEX_C)
