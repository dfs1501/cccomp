#ifndef UNARY_OP_H
#define UNARY_OP_H

#include "Expression.h"

class UnaryOp : public Expression {
public:
  Expression* expr;
  
  UnaryOp(Expression* expr) {
    this->expr = expr;
  }
};

#endif
