#ifndef MINUS_H
#define MINUS_H

#include "BinaryOp.h"

class Minus : public BinaryOp {
public:
  using BinaryOp::BinaryOp;
  
  ACCEPT_VISITOR
};

#endif