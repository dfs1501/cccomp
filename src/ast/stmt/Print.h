#ifndef PRINT_H
#define PRINT_H

#include <string>

#include "SingleStatement.h"
#include "../expr/Expression.h"

class Print : public SingleStatement {
public:
  Expression *expr;
  
  Print(Expression *expr): expr (expr) {}
  
  ACCEPT_VISITOR
};

#endif
