#ifndef IDENT_H
#define IDENT_H

#include <string>

#include "Expression.h"
#include "../../semantics/Environment.h"

using namespace std;

class Ident : public Expression {
public:
  string name;
  string mangledName;
  
  Class *pClass;
  
  Ident (string name) : name(name), mangledName(name), pClass (nullptr) {}
  
  ACCEPT_VISITOR
};

#endif
