#ifndef SUBSCRIPT_H
#define SUBSCRIPT_H

#include "Expression.h"

using namespace std;

class Subscript : public Expression {
public:
  Expression *expr;
  Expression *subscript;
  
  Subscript (Expression *expr, Expression *subscript):
  expr (expr),
  subscript (subscript)
  {}
  
  ACCEPT_VISITOR
};

#endif
