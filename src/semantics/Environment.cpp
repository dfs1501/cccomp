#include "Environment.h"
#include "TType.h"


Variable::Variable (): pAst (nullptr), pType (new TUnassigned ()) {}

Method::Method (): pEnvironment (new Environment ()), pAst (nullptr), pReturnType (new TUnassigned ()) {}

Class::Class (): pEnvironment (new Environment ()), pAst (nullptr), pMainClass (nullptr), pBaseClass (nullptr), pType (new TUnassigned ()) {}

Variable* Environment::findVar (const string& name, bool localOnly) {
  Environment *pEnv = this;
  while (pEnv) {
    auto it = pEnv->vars.find (name);
    if (it != pEnv->vars.end ()) {
      return it->second;
    }
    // Look in base class
    if (pEnv == pCurrentClass->pEnvironment && pCurrentClass->pBaseClass) {
      auto inherited = pCurrentClass->pBaseClass->pEnvironment->findVar (name, localOnly);
      if (inherited) {
        return inherited;
      }
    }
    if (localOnly) {
      break;
    }
    pEnv = pEnv->pParent;
  }
  return nullptr;
}
Method* Environment::findMethod (const string& name, bool localOnly) {
  Environment *pEnv = this;
  while (pEnv) {
    auto it = pEnv->methods.find (name);
    if (it != pEnv->methods.end ()) {
      return it->second;
    }
    // Look in base class
    if (pEnv == pCurrentClass->pEnvironment && pCurrentClass->pBaseClass) {
      auto inherited = pCurrentClass->pBaseClass->pEnvironment->findMethod (name, localOnly);
      if (inherited) {
        return inherited;
      }
    }
    if (localOnly) {
      break;
    }
    pEnv = pEnv->pParent;
  }
  return nullptr;
}
Class* Environment::findClass (const string& name, bool localOnly) {
  Environment *pEnv = this;
  while (pEnv) {
    auto it = pEnv->classes.find (name);
    if (it != pEnv->classes.end ()) {
      // Can't use main class as type
      if (it->second->pMainClass) {
        return nullptr;
      }
      return it->second;
    }
    if (localOnly) {
      break;
    }
    pEnv = pEnv->pParent;
  }
  return nullptr;
}

Class* Environment::tryDeclareClass (const string& name, Environment *pEnvironment, int mangleID) {
  if (findClass (name, true)) {
    return nullptr;
  }
  auto c = new Class ();
  c->name = name;
  c->mangleID = mangleID;
  c->pEnvironment->pParent = pEnvironment;
  c->pEnvironment->pCurrentClass = c;
  classes [name] = c;
  return c;
}

Method* Environment::tryDeclareMethod (const string& name, Environment *pEnvironment, int mangleID) {
  if (findMethod (name, true)) {
    return nullptr;
  }
  auto m = new Method ();
  m->name = name;
  m->mangleID = mangleID;
  m->pEnvironment->pParent = pEnvironment;
  m->pEnvironment->pCurrentClass = pEnvironment->pCurrentClass;
  m->pEnvironment->pCurrentMethod = m;
  methods [name] = m;
  return m;
}

Variable* Environment::tryDeclareVariable (const string& name, Class *pClass, int mangleID) {
  if (findVar (name, true)) {
    return nullptr;
  }
  if (findClass (name)) {
    return nullptr;
  }
  if (findMethod (name, true)) {
    return nullptr;
  }
  auto v = new Variable ();
  v->name = name;
  v->mangleID = mangleID;
  v->pClass = pClass;
  vars [name] = v;
  return v;
}
